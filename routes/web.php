<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Voyager::routes();

    Route::get('/reports', 'ReportController@index');
});



// Route::get('admin/so_report', 'SoReportController@index');
Route::get('admin/report_header', 'createAnewReport@index')->middleware('admin.user');
Route::post('admin/report_header/create_new_report', 'createAnewReport@create')->middleware('admin.user');
Route::get('admin/admin/report_header/delete/{id}', 'createAnewReport@destroy')->middleware('admin.user');
//  Route::post('admin/report_header/edit_report_header','createAnewReport@update')->middleware('admin.user');
Route::get('admin/report_header/edit/{id}', 'createAnewReport@view')->middleware('admin.user');

//Untuk Balance set Condition
Route::get('admin/balance_set_condition_report', 'balanceReportController@index')->middleware('admin.user');
Route::post('admin/balance_set_condition_report/create_new_report', 'balanceReportController@create')->middleware('admin.user');
Route::get('admin/balance_set_condition_report/delete/{id}', 'balanceReportController@destroy')->middleware('admin.user');
Route::get('admin/balance_set_condition_report/view/{id}', 'balanceReportController@view')->middleware('admin.user');
Route::get('admin/balance_set_condition_report/edit/{id}', 'balanceReportController@edit')->middleware('admin.user');
Route::put('admin/balance_set_condition_report/edit/update/{id}', 'balanceReportController@update')->middleware('admin.user');
//  Route::get('admin/search','balanceReportController@search')->middleware('admin.user');

//input production
Route::get('admin/production', 'ProductionResultController@index')->middleware('admin.user');
Route::post('admin/production/create_new_production', 'ProductionResultController@create')->middleware('admin.user');
Route::get('admin/production/view/{id}', 'ProductionResultController@view')->middleware('admin.user');
Route::get('admin/production/delete/{id}', 'ProductionResultController@destroy')->middleware('admin.user');
Route::get('admin/production/edit/{id}', 'ProductionResultController@edit')->middleware('admin.user');
Route::put('admin/production/edit/update/{id}', 'ProductionResultController@update')->middleware('admin.user');

// untuk approval
// Route::get('admin/approval', 'ApproveController@index');

//untuk trial ng set
Route::get('admin/trial_ng_set', 'NgsetController@index')->middleware('admin.user');
Route::post('admin/trial_ng_set/create_new_report', 'NgsetController@create')->middleware('admin.user');
Route::get('admin/trial_ng_set/view/{id}', 'NgsetController@view')->middleware('admin.user');
Route::get('admin/trial_ng_set/delete/{id}', 'NgsetController@destroy')->middleware('admin.user');
Route::get('admin/trial_ng_set/edit/{id}', 'NgsetController@edit')->middleware('admin.user');
Route::put('admin/trial_ng_set/edit/update/{id}', 'NgsetController@update')->middleware('admin.user');

//untuk change member report
Route::get('admin/change_member', 'CMRController@index')->middleware('admin.user');
Route::post('admin/change_member/create_new_report', 'CMRController@create')->middleware('admin.user');
Route::get('admin/change_member/view/{id}', 'CMRController@view')->middleware('admin.user');
Route::get('admin/change_member/edit/{id}', 'CMRController@edit')->middleware('admin.user');
Route::put('admin/change_member/edit/update/{id}', 'CMRController@update')->middleware('admin.user');
Route::get('admin/change_member/delete/{id}', 'CMRController@destroy')->middleware('admin.user');

//so report
Route::get('/admin/so_report', 'SoReportController@index')->middleware('admin.user');
Route::post('/admin/so_report/create_new_report', 'SoReportController@create')->middleware('admin.user');
Route::get('/admin/so_report/edit/{id}', 'SoReportController@edit')->middleware('admin.user');
Route::put('/admin/so_report/edit/update/{id}', 'SoReportController@update')->middleware('admin.user');
Route::get('/admin/so_report/view/{id}', 'SoReportController@view')->middleware('admin.user');
Route::get('/admin/so_report/delete/{id}', 'SoReportController@destroy')->middleware('admin.user');

//untuk change model report
Route::get('admin/change_model', 'ChangeModelController@index')->middleware('admin.user');
Route::post('admin/change_model/create_new_report', 'ChangeModelController@create')->middleware('admin.user');
Route::get('admin/change_model/delete/{id}', 'ChangeModelController@destroy')->middleware('admin.user');

/*  
**  created by Yunus
**  on 05 Juni 2020
*/
//  ini untuk contoh
// Route::get('download_test','ReportController@index');
/** ---end ini untuk contoh---*/

Route::get('download_loss_time_reports', 'ReportController@loss_time_report');
Route::get('download_change_model_reports', 'ReportController@change_model_report');
Route::get('download_trial_ng_set_reports', 'ReportController@trial_ng_set_report');
Route::get('download_change_member_reports', 'ReportController@change_member_report');
Route::get('download_balance_sets_condition_reports', 'ReportController@balance_sets_condition_report');
Route::get('download_production_result_reports', 'ReportController@production_result_reports');
Route::get('download_operation_summary_reports', 'ReportController@operation_summary_reports');
/** ---end of yunus---*/


//buat route dulu
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/approved', 'ApprovalController@approved');
Route::get('/userlogin', 'UserController@index');
