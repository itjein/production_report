# password macro ini = PROD1234



Sub load_file()
    With Application.FileDialog(msoFileDialogOpen)
        .AllowMultiSelect = False
        .FilterIndex = 3
        .InitialFileName = "\\svrfile\SHARING\A.PRODUCTION_ DATABASE\DATA_ DOCUMENT\A_OP_SMRY\OP_SMRY_MACRO"
        .Show
        Range("c3").ClearContents
        If (.SelectedItems.Count > 0) Then
            Cells(3, 3) = .SelectedItems.Item(1)
        End If
        
        'Range("i2:i300").ClearContents
        'For i = 1 To .SelectedItems.Count
        '    Cells(i + 1, 10) = .SelectedItems.Item(i)
        'Next i
    End With
End Sub

Sub save_file()
    If (Cells(3, 3) = "") Then
        'create a message box
        MsgBox prompt:="Klik (STEP 1) terlebih Dahulu !"
        Exit Sub
    End If
    
    Dim sFolder As String
    ' Open the select folder prompt
    With Application.FileDialog(msoFileDialogFolderPicker)
        .InitialFileName = "\\svrfile\SHARING\A.PRODUCTION_ DATABASE\DATA_ DOCUMENT\A_OP_SMRY"
        If .Show = -1 Then ' if OK is pressed
            sFolder = .SelectedItems(1)
        End If
    End With
    
    If sFolder <> "" Then ' if a file was chosen
        Range("c6").ClearContents
        Cells(6, 3) = sFolder
    End If
End Sub

Sub generate_data()
    Worksheets("macros").Activate
    If (Cells(3, 3) = "") Then
        'create a message box
        MsgBox prompt:="Klik (STEP 1) terlebih Dahulu !"
        Exit Sub
    End If
    
    If (Cells(6, 3) = "") Then
        'create a message box
        MsgBox prompt:="Klik (STEP 2) terlebih Dahulu !"
        Exit Sub
    End If
    
    'Check all worksheet in operation summary
    'Dim opsum_wb As Workbook
    Dim Ws_opsum_count As Integer
    Dim i As Integer
    Dim opsum_ws As String
    Dim opsum_header As String
    
    this_wb = ActiveWorkbook.Name
    
    
    'Open file location operation summary download
    Workbooks.Open Filename:=Cells(3, 3), ReadOnly:=True
    opsum_wb = ActiveWorkbook.Name
    
    
    
    'check total worksheet di opsum
    Ws_opsum_count = ActiveWorkbook.Worksheets.Count
    
    'Looping copy content sheet operation summary ke template
    For i = 1 To Ws_opsum_count
        'MsgBox Prompt:=ActiveWorkbook.Worksheets(i).Name
        opsum_ws = Workbooks(opsum_wb).Worksheets(i).Name
        opsum_header = Left(opsum_ws, Len(opsum_ws) - 2)
        'MsgBox Prompt:=opsum_ws
        'MsgBox Prompt:=opsum_ws & "=>" & WorksheetExists(opsum_ws, Workbooks(this_wb))
        If (WorksheetExists(opsum_header, Workbooks(this_wb)) = True) Then
            Workbooks(opsum_wb).Sheets(opsum_ws).Range("F6:BD6").Copy _
                Workbooks(this_wb).Sheets(opsum_header).Range("F6:BD6")
            Workbooks(this_wb).Sheets(opsum_header).Range("A2:BD2").Copy _
                Workbooks(opsum_wb).Sheets(opsum_ws).Range("A2:BD2")
        End If
        
        If (WorksheetExists(opsum_ws, Workbooks(this_wb)) = False) Then
            Call CopySheetToThisWorkbook(opsum_ws, Workbooks(opsum_wb), Workbooks(this_wb))
        Else
            'clear sheet template
            Workbooks(this_wb).Sheets(opsum_ws).Cells.Clear
            
            'copy paste from opsum to template
            Workbooks(opsum_wb).Sheets(opsum_ws).Range("A1:CA200").Copy _
                Workbooks(this_wb).Sheets(opsum_ws).Range("A1:CA200")
        End If
    Next i
    
    Workbooks(opsum_wb).Close SaveChanges:=False
    
    'save as workbook generate
    Dim y As Integer
    Dim m As String
    Dim d As String
    Dim h As String
    Dim min As String
    Dim s As String
    Dim fn As String
    Dim tempath As String
    
    y = Year(Now)
    m = Format(Month(Now), "00")
    d = Format(Day(Now), "00")
    h = Format(Hour(Now), "00")
    min = Format(Minute(Now), "00")
    s = Format(Second(Now), "00")
    'MsgBox Prompt:=y & m & d & h & min & s
    
    'fileformat
    'https://docs.microsoft.com/en-us/office/vba/api/excel.xlfileformat
    'MsgBox prompt:=Workbooks(this_wb).Worksheets("macros").Cells(6, 3)
    tempath = Workbooks(this_wb).Worksheets("macros").Cells(6, 3)
    fn = tempath & "\" & "GENERATE_OPR_SMRY_" & y & m & d & h & min & s
    'Application.Wait (Now + TimeValue("0:00:03"))
    Application.DisplayAlerts = False
    With Workbooks(this_wb)
        .CheckCompatibility = False
        .SaveAs Filename:=fn & ".xls", FileFormat:=56
        .CheckCompatibility = True
    End With
    Application.DisplayAlerts = True
    
    'Workbooks(this_wb).SaveAs Filename:=fn, FileFormat:=56
    Dim answer As Integer
    answer = MsgBox("GENERATE DONE !", vbQuestion + vbOK)
    If answer = vbOK Then
        'Application.DefaultSaveFormat = SaveFormat
        'MsgBox "You can find the file in " & Workbooks(this_wb).Worksheets("macros").Range("C6").Value
        Exit Sub
    End If
    
    'Dim answer As Integer
    'answer = MsgBox("GENERATE DONE !", vbQuestion + vbOK)
    'If answer = vbOK Then
    '  Workbooks(this_wb).Close SaveChanges:=False
    'End If
    
    
End Sub

Sub reset()
    Range("c3").ClearContents
    Range("c6").ClearContents
End Sub
    

Public Sub CopySheetToThisWorkbook(from_sheet As String, from_wb As Workbook, to_wb As Workbook)
    from_wb.Sheets(from_sheet).Copy _
    After:=to_wb.Worksheets(to_wb.Worksheets.Count)
End Sub
Function WorksheetExists(shtName As String, Optional wb As Workbook) As Boolean
    Dim sht As Worksheet

    If wb Is Nothing Then Set wb = ThisWorkbook
    On Error Resume Next
    Set sht = wb.Sheets(shtName)
    On Error GoTo 0
    WorksheetExists = Not sht Is Nothing
End Function




