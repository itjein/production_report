<?php

namespace App;

use App\Observers\ModelObserver;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CrudModel extends Model
{
    // protected $table = 'production_results';
    protected static function boot()
    {
        // you MUST call the parent boot method 
        // in this case the \Illuminate\Database\Eloquent\Model
        parent::boot();

        // note I am using static::observe(...) instead of Config::observe(...)
        // this way the child classes auto-register the observer to their own class
        static::observe(ModelObserver::class);
    }

    public function getColumns()
    {
        $query = "SELECT COLUMN_NAME, IS_NULLABLE, DATA_TYPE , CHARACTER_MAXIMUM_LENGTH
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = '{$this->table}' ";

        if (env('DB_CONNECTION') == 'sqlsvr') {
            $query .= "AND TABLE_SCHEMA='dbo'";
        }

        return DB::select($query);
    }

    public function getColumnList()
    {
        return Schema::getColumnListing($this->getTable());
    }

    public function getRequiredColumn()
    {
        $data = $this->getColumns();

        $res = [];
        foreach ($data as $key => $value) {
            $res[$value->COLUMN_NAME] = $value->IS_NULLABLE == "NO";
        }

        return $res;
    }
}
