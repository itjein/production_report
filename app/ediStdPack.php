<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ediStdPack extends Model
{
    protected $connection = 'sqlsvr3';
    protected $table = 'StdPack';
}