<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class StatusConfig extends CRUDModel
{
    protected $table = 'status_configs';
    public function save(array $options = [])
    {
        $this->created_by = Auth::user()->id;
        $this->updated_by = Auth::user()->id;
        parent::save($options);
    }
}
