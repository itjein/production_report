<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class FillReport extends AbstractAction
{
    public function getTitle()
    {
        return 'Fill Report';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right mx-2',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.production-results.index', ['report_header_id' => 1]);
    }
}
