<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StdTime extends Model
{
    //
    public function __construct()
    {
        $this->connection = env("DB_CONNECTION4", "sqlsvr4");
    }

    protected $table = "vStdTimeSum";

    protected $casts = [
        "TOTAL_STD_TIME" => "float"
    ];
}
