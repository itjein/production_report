<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ApprovalsConfig extends CrudModel
{
    protected $table = 'approvals_config';
    public function save(array $options = [])
    {
        $this->user_create = Auth::user()->id;
        parent::save($options);
    }
}
