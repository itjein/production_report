<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndirectOpHour extends CrudModel
{
    protected $table = 'indirect_op_hours';

    public function operation_summary()
    {
        return $this->belongsTo('App\OperationSummary');
    }



    // indirect_op_hours_total
    public function getOpHoursTotal()
    {
        $id = $this->operation_summary_id;

        return $this->where('operation_summary_id', $id)
            ->whereIn('type', [
            'ANNUAL LEAVE', 'MEDICAL LEAVE'
            ])
        ->groupBy('operation_summary_id')->sum('value');
    }

    public function save(array $options = [])
    {
        parent::save($options);

        try {
            //code...
            $opSummary = $this->operation_summary;

            if (!\is_null($opSummary)) {
                $opSummary->indirect_op_hours_total = $this->getOpHoursTotal();
                // $opSummary->setFixedTimeAttribute('-'); //di op summary save, sudah panggil setFixedTimeAttribute
                $opSummary->save();
            }
        } catch (\Exception $th) {
            //throw $th;
        }
    }

    protected static function boot() {
      parent::boot();
      static::deleting(function($indirectOpHour) {
        try {
            //code...
          $opSummary = $indirectOpHour->operation_summary;
          if (!\is_null($opSummary)) {
            $opSummary->indirect_op_hours_total = $indirectOpHour->getOpHoursTotal();
                // $opSummary->setFixedTimeAttribute('-'); //di op summary save, sudah panggil setFixedTimeAttribute
            $opSummary->save();
          }
        } catch (\Exception $th) {
            //throw $th;
        }
      });
  }
}
