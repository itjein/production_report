<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ReportsHeader;
use Illuminate\Support\Facades\Auth;
use DB;

class Line extends CrudModel
{

    protected $casts = [
        'efficiency' => 'float',
        'productivity' => 'float',
        'rate_of_operation' => 'float',
        'rate_of_attendance' => 'float',
    ];

    public function save(array $options = [])
    {
        $this->create_user = Auth::user()->id;
        parent::save($options);
    }
}
