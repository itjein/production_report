<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class Schedule extends Model
{
    protected $connection = 'sqlsvr2';
    public function __construct()
    {
        // koneksi ke database schedule ario
        $this->connection = 'sqlsvr2'; //env('DB_CONNECTION2', 'sqlsrv2');
    }

    protected $table = 'tbl_sch_detail';

    // protected $table = 'schedules';
}
