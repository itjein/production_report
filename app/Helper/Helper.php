<?php
namespace App\Helper;

use Illuminate\Database\Eloquent\Builder;

class Helper {

    public static function getEloquentSqlWithBindings(Builder $query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
    public static function generateLineForSheetName($name){
        $explode_line   = explode(" ",$name);
        $line_name      = $explode_line[0];
        $line_no        = ltrim($explode_line[1],"0");
        return "{$line_name} {$line_no}";
    }
    public static function generateSheetNameGroup($name,$group){
        
        $line = Helper::generateLineForSheetName($name);
        return "{$line}-{$group}";
    }

}