<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReportsHeader;
use App\Approval;
use App\Http\Controllers\Controller;

class ApproveController extends Controller
{
    public function index(){
        //panggil yang ada di database
        $data =Approval::all();
        $dropdown = ReportsHeader::get();
       
        return view('admin.vendor.approval.index',compact('data','dropdown'));
    }
    public function create(Request $request){
       
        ReportsHeader::create($request->all());

        return redirect('/admin/approval');
    }
}
