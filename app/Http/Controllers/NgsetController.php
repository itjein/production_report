<?php

namespace App\Http\Controllers;
use App\TrialNgSet;
use App\ReportsHeader;
use Illuminate\Http\Request;

class NgsetController extends Controller
{
    public function index(){
        $data = TrialNgSet::orderBy('id','desc')->get();
        $dropdown = ReportsHeader::get();
        return view('admin.vendor.trial_ng_set.index',compact('data','dropdown'));
    }
    public function create(Request $request){
        TrialNgSet::create($request->all());
        return redirect('/admin/trial_ng_set');
    }
    public function view($id){
        $data = TrialNgSet::findOrFail($id);
        return view('admin.vendor.trial_ng_set.view',compact('data'));

    }
    public function destroy($id){
        \Session::flash('flash_message','data berhasil di hapus');
        TrialNgSet::where('id',$id)->delete();
        return redirect('/admin/trial_ng_set');
    }
    public function edit($id){
        $data=TrialNgSet::findOrFail($id);
        return view('admin.vendor.trial_ng_set.edit',compact('data'));
    }
    
    public function update($id, Request $request)
    {
        $trial =TrialNgSet::findorFail($id);
        $trial->report_id = $request->report_id;
        $trial->symptom = $request->symptom;
        $trial->result = $request->result;
        $trial->shift = $request->shift;
        $trial->save();
        return redirect('/admin/trial_ng_set');
}



}
 