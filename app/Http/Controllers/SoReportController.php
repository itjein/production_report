<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReportsHeader;
use App\SoReport;
use App\ProductionResult;
use App\LossTime;
use App\Dic;
use DB;

class SoReportController extends Controller
{
    public function index(){
        $data= SoReport::orderBy('id', 'asc')->get();
        $rep= ReportsHeader::get();
        $rows= ProductionResult::get();
        $row= LossTime::get();
        $dep= Dic::get();
        return view('admin.vendor.SoReport.index', compact('data','rep','rows','row','dep'));
    }
    public function create(Request $request){
        SoReport::create($request->all());

        return redirect('/admin/so_report');
    }

    public function edit($id){
        $data=SoReport::findOrFail($id);

        return view('admin.vendor.SoReport.index', compact('data'));
    }

    public function update(Request $request ,$id){
        $data=SoReport::findOrFail($id);
        $data->model = $request->model;
        $data->lot_no = $request->lot_no;
        $data->time_start = $request->time_start;
        $data->time_end = $request->time_end;
        $data->hours = $request->hours;
        $data->mp_d1 = $request->mp_d1;
        $data->code = $request->code;
        $data->dic = $request->dic;
        $data->save();
        return redirect('/admin/so_report');
    }

    public function view($id){
        $data=SoReport::findOrFail($id);

        return view('admin.vendor.SoReport.index', compact('data'));
    }

    public function destroy($id){
        SoReport::where('id',$id)->delete();

        return redirect('/admin/so_report');
    }


}
