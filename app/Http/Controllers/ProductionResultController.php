<?php

namespace App\Http\Controllers;

use App\ProductionResult;
use App\ReportsHeader;
use Illuminate\Http\Request;
use DB;

class ProductionResultController extends CrudController
{
    protected $model = 'App\\ProductionResult';
}
