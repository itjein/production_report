<?php

namespace App\Http\Controllers;

use App\Dic;
use App\LossTimeReport;
use Illuminate\Http\Request;
use App\ediStdPack;
use App\ReportHeader;
use Illuminate\Support\Facades\DB;

class LossTimeReportController extends CrudController
{
    protected $model = 'App\\LossTimeReport';

    public function getRequiredColumnManual()
    {
        $requiredColumns = [
            "a.report_header_id" => "true",
            "a.group" => "true",
            "a.loss_code" => "true",
            "description" => "true",
            "grand_total" => "true"
        ];

        return $requiredColumns;
    }
    public function getIndOphLostTime($id)
    {
        $indOpHLostTime = DB::select("SELECT DISTINCT 
                                a.report_header_id,
                                a.[group], 
                                a.loss_code, 
                                concat(c.code,'.',c.[description]) as description,
                            (
                                SELECT SUM(b.hours * b.man_power)
                                FROM loss_time_reports b
                                WHERE b.report_header_id = '{$id}'
                                    AND b.loss_code = a.loss_code
                                    AND b.[group] = a.[group]
                            ) AS grand_total
                            FROM loss_time_reports a
                                LEFT JOIN loss_times c ON c.id = a.loss_code
                            WHERE a.report_header_id = '{$id}'
                            ORDER BY a.[group];");
        $count = strval(count($indOpHLostTime));
        return json_encode([
            "success" => "true",
            "total" => $count,
            "data" => $indOpHLostTime
        ]);
    }
    public function getPartEdi(Request $request)
    {
        $allowParams = ['partnumber'];
        $data = ediStdPack::join('supplier', 'stdpack.suppcode', '=', 'supplier.suppcode')
                ->select('stdpack.suppcode', DB::raw('RTRIM(LTRIM(supplier.suppname)) as suppname'), 'stdpack.partnumber', DB::raw('RTRIM(LTRIM(stdpack.partname)) as partname'))
                ->distinct()
                ->take(15);
                
        if ($request->toArray()) {
            foreach ($allowParams as $value) {
                $data = $data->where($value, 'like',  $request->$value . '%');
            }
        }
        $data = $data->orderBy('partnumber', 'asc')->get();
        
        return [
            "success" => true,
            "data" => $data,
        ];
    }

    public function requiredColSummary(){

    }

    
    protected $joinColumnLists = [
        "report_date",
        "time_start",
        "time_end",
        "lost",
        "line",
        "model",
        "prod_no",
        "N1",
        "responsible",
        "symptom",
        // "total_time",//ga bisa di filter karena ini sum dari dua kolom
        "man_power",
        "dic",
        "dept_name",
        "disposal",
        "part_no",
    ];
    
    protected $aliases = [
        "total_time" => "hours * man_power",
        "lost" => "hours",
        "prod_no" => "lotno",
        "N1" => "loss_code",
        "responsible" => "loss_code_name",
        "dic" => "dic.dept_code",
        "dept_name" => "dic.dept_name",
    ];
    public function getSummary(Request $request){
        // $today = date("Y-m-d");
        // return LossTimeReport::get();
        // return Dic::get();
        $lossTimeTable = (new LossTimeReport)->getTable();
        $data = LossTimeReport::select([
            "report_date", //report header
            "time_start",
            "time_end",
            "hours as lost", //as lost
            "line", //diambil dari report header
            "model",
            "lotno as prod_no",
            "loss_code as N1", //N1
            "loss_code_name as responsible", //responsible
            'symptom',
            // DB::raw("hours * man_power  as total_time"), //t.time ?? entah diambil dari mana
            DB::raw("cast( round(hours * man_power, 2) as decimal(36,2)) as total_time"), //t.time ?? entah diambil dari mana
            "man_power", //m.p ??? entah diambil dari mana
            "dic.dept_code as dic",
            "dic.dept_name as dept_name",
            "disposal",
            "part_no", 
        ])->join('report_headers', $lossTimeTable . ".report_header_id", '=', 'report_headers.id')
            ->leftJoin('dic', 'dic.id', '=', $lossTimeTable.'.dic')
        ->where(function($q) use ($request) {
            if($request->has('start_date') && $request->has('end_date')) {
                $q->whereBetween('report_headers.report_date', [$request->get('start_date') , $request->get('end_date')]);
            }
        });


        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            if (!\is_null($query)) { //kalau not null, baru foreach
                $columns = $this->joinColumnLists;
                foreach ($query as $key => $value) {
                    if (\in_array($key, $columns)) {
                        if (isset($this->foreign[$key])) {
                            $data = $data->where($key, $value);
                        } else {
                            if(isset($this->aliases[$key])){
                                $data = $data->where($this->aliases[$key], 'like', "%{$value}%");
                            }else{
                                $data = $data->where($key, 'like', "%{$value}%");
                            }
                        }
                    }
                }
            }
        }

        if($request->has('orderBy')){
            // disini harusnya dicek dulu kolomnya ada apa nggak
            $allowed = [
                "report_date",
                "time_start",
                "time_end",
                "lost",
                "line",
                "model",
                "prod_no",
                "N1",
                "responsible",
                "symptom",
                "total_time",
                "man_power",
                "dic",
                "dept_name",
                "disposal",
                "part_no",
            ];
        
            if(in_array($request->get('orderBy'), $allowed )){
                $data = $data->orderBy($request->get('orderBy') , $request->get('ascending') ? 'asc' : 'desc');
            }
        }else{
            $data = $data->orderBy('report_headers.report_date', 'desc');
        }

        $data = $data->paginate($request->limit);

        return $data;

    }
}