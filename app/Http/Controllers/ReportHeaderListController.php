<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Approval;
use App\ProductionResult;
use DB;
use App\Helper\Helper;

class ReportHeaderListController extends CrudController
{
    protected $model = 'App\\ReportHeader';

    public function index(Request $request)
    {
        $data = (new $this->model)
        ->select(
            'report_headers.*',
            'approvals.status',
            'approvals.approval_time',
            'users.user_id',
            // 'users.name',
            DB::raw("[name] = STUFF(
                (
                    SELECT ', ' + usr.name
                    FROM users usr
                    WHERE usr.id IN
                    (
                    select user_approve from approvals where report_header_id = report_headers.id
                    ) FOR XML PATH(''), TYPE
                ).value('.', 'NVARCHAR(MAX)'), 1, 1, '')")
        )->leftJoin('approvals', function ($join) {
            $join->on('approvals.report_header_id', '=', 'report_headers.id')
                ->whereRaw('approvals.status = (select max(status) from approvals where report_header_id = report_headers.id)');
        })
        ->withCount([
            'production_results',
            'lost_time_reports',
            'change_model_reports',
            'trial_ng_sets',
            'change_member_reports',
            'summaries',
            'IndirectOpHours',
            'actual_direct_op_hours'
        ])
        ->leftJoin("users", "users.id", "=", "approvals.user_approve");

        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            // $columns = $this->getColumnList();
            
            $columns = [
                "id" => "report_headers.id",
                "line" =>  "report_headers.line",
                "schedule_date" => "report_headers.schedule_date",
                "report_date" => "report_headers.report_date",
                "created_by" => "report_headers.created_by",
                "created_at" => "report_headers.created_at",
                "updated_at" => "report_headers.updated_at",
                "status" => "approvals.status",
                "approval_time" => "approvals.approval_time",
                "user_id" => "users.user_id",
                // "name" => "users.name"
            ];
            
            foreach ($query as $key => $value) {
                if (isset($this->foreign[$key])) {
                    $data = $data->where($key, $value);
                } else {
                    if(isset($columns[$key])){
                        $data = $data->where($columns[$key], 'like', "%{$value}%");
                    }

                    if (strpos($key, '_count') !== false) {
                        $newKey = \str_replace('_count', '', $key);
                        $data = $data->has( $newKey, $value);
                    }
                }
            }
        }

        // order of column
        if ($request->has('orderBy')) {
            $ascending = $request->has('ascending') && ($request->get('ascending') == 1) ?
                'asc' : 'desc';
            $data = $data->orderBy($request->get('orderBy'), $ascending);
        }
        // return Helper::getEloquentSqlWithBindings($data);
        $data = $data->paginate($request->has('limit') ? $request->limit : 15);

        // laravel collections
        $results = collect([
            'success' => true,
            'request' => $request->all()
        ]);

        return  $results->merge($data);
    }
}