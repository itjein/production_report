<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigController extends Controller
{
    //
    public function index(Request $request){
        return [
            "success" => true,
            "data" => [
                "std_time_url" => setting("admin.std_time_url", "http://136.198.117.80/std_time/api/public/summaries")
            ]
        ];
    }
}
