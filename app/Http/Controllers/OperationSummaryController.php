<?php

namespace App\Http\Controllers;

use App\OperationSummary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OperationSummaryController extends CrudController
{
    protected $model = 'App\OperationSummary';

    public function index(Request $request)
    {
        $data = (new $this->model)->select();

        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            $columns = $this->getColumnList();
            foreach ($query as $key => $value) {
                if (\in_array($key, $columns)) {
                    if (isset($this->foreign[$key])) {
                        $data = $data->where($key, $value);
                    } else {
                        $data = $data->where($key, 'like', "%{$value}%");
                    }
                }
            }
        }

        // order of column
        if ($request->has('orderBy')) {
            $ascending = $request->has('ascending') && ($request->get('ascending') == 1) ?
                'asc' : 'desc';
            $data = $data->orderBy($request->get('orderBy'), $ascending);
        }

        $data = $data->paginate($request->has('limit') ? $request->limit : 15);

        foreach ($data as $key => $value) {
            $this->computeValue($value);
        }

        // laravel collections
        $results = collect([
            'success' => true,
            'request' => $request->all()
        ]);

        return  $results->merge($data);
    }

    public function computeValue(OperationSummary &$opSummary)
    {
        $opSummary->initComputedValue();
        return $opSummary;
    }
}
