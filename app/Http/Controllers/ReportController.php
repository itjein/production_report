<?php

namespace App\Http\Controllers;

use App\Exports\DataExport;
use App\Helper\Helper;
use App\MasterLossTime;
use App\OperationSummary;
use App\ProductionResult;
use App\ReportHeader;
use App\ChangeModelReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use DB;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //  ini untuk contoh
    // public function getData()
    // {
    //     return ['teguh', 'harris', 'ario'];
    // }
    /** ---end ini untuk contoh---*/


    /*  
    **  created by Yunus
    **  on 05 Juni 2020
    **  note:   menambah function index (15 Juni 2020)
    */
    protected $threshold = 13;
    protected $colCount = 6;

    public function index(Request $request)
    {
        $lines  = DB::select("select line from lines order by line asc");
        /* 
            menu apa aja yang muncul di download;
        */
        $menus = $this->getMenus();
        return view('admin.report', compact('lines', 'menus'));
    }

    public function getData(Model $model, Request $request,$reportType = null){
        $allowedParams = ['start_date', 'end_date', 'line'];

        $params = $request->only($allowedParams);

        $model = new $model;

        $tableName = $model->getTable();

        $from = $request->has('start_date') ? $request->get('start_date') : Carbon::today()->firstOfMonth()
            ->format("Y-m-d");
        $to = $request->has('end_date') ? $request->get('end_date') : Carbon::today()->format("Y-m-d");

        $selectQuery = $model
                        ->select([
                            "{$tableName}.*",
                            "report_headers.report_date",
                            "report_headers.line"
                        ]);
        
        if($reportType == "loss_time_reports"){
            $selectQuery = $model->select(
                                "report_headers.report_date AS Date",
                                "{$tableName}.time_start AS Time Start",
                                "{$tableName}.time_end AS Time End",
                                DB::raw("CAST({$tableName}.hours AS DECIMAL(10,3)) AS Lost"),
                                "report_headers.line",
                                "{$tableName}.model AS Model",
                                "{$tableName}.lotno AS Prod.No",
                                DB::raw("SUBSTRING({$tableName}.loss_code_name,1,1) AS N1"),
                                DB::raw("SUBSTRING({$tableName}.loss_code_name,4,LEN({$tableName}.loss_code_name)) AS Responsible"),
                                "{$tableName}.symptom AS Symptom",
                                DB::raw("CAST(({$tableName}.hours * {$tableName}.man_power) AS DECIMAL(10,3)) AS TTime"),
                                "{$tableName}.man_power AS MP",
                                DB::raw("(select top 1 dept_name from dic where id = {$tableName}.dic) AS DIC"),
                                "{$tableName}.disposal AS Disposal",
                                "{$tableName}.part_no AS Part Number",
                                "{$tableName}.action AS Action"
                            );
        }                    
                            
        $query = $selectQuery
            ->join('report_headers', "{$tableName}.report_header_id", "=", 'report_headers.id')
            ->whereBetween('report_headers.report_date', [ $from, $to ])
            ->where(function($query) use ($request){
                if($request->has('line')) {
                    if(is_array($request->line)){
                        $query->whereIn('report_headers.line', $request->get('line'));
                    }else{
                        $query->where('report_headers.line', $request->get('line'));
                    }
                }
            });

            if($reportType == "loss_time_reports"){
                return $query;
            }

            return $query->get();

    }

    public function generateData(Model $model, Request $request, $filename = "", $dataParams = null){
        if($filename == '') {
            $filename = $model->getTable();
        }

        // create no dokumen
        // $noDoc = collect([ "SJ2-04S-04F26 Rev. 3" ]);
        // $noDoc = collect($this->getData($model, $request));

        if($dataParams == null) {
            // $dataToExport = $noDoc->merge($this->getData($model, $request));
            $dataToExport = $this->getData($model, $request);
        }else{
            // $dataToExport = $noDoc->merge($dataParams);
            $dataToExport = $dataParams;
        }
        // return $dataToExport;

       

        try {
            //code...
            return Excel::download(new DataExport($dataToExport) , "{$filename}.xlsx" );
        } catch (\Exception $th) {
            return [
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
    }

    public function getMenus(){
        return [
            [
                'title' => "loss time report",
                "action_url" => "../download_loss_time_reports"
            ],
            [
                'title' => "Change Model Report",
                "action_url" => "../download_change_model_reports",
                "cgmdl" => true 
            ],
            [
                'title' => "Trial NG Set Report",
                "action_url" => "../download_trial_ng_set_reports"
            ],
            [
                'title' => "Change Member Report",
                "action_url" => "../download_change_member_reports"
            ],
            [
                'title' => "Balance Sets Condition Report",
                "action_url" => "../download_balance_sets_condition_reports"
            ],
            [
                'title' => "Production Result Report",
                "action_url" => "../download_production_result_reports"
            ],
            [
                'title' => "Operation Summary Report",
                "action_url" => "../download_operation_summary_reports",
                "date" => false,
                "opsum" => true
                // "multiple" => false,
            ],

        ];
    }

    public function loss_time_report(Request $request)
    {
        $model      = new \App\LossTimeReport;
        $tableName  = $model->getTable();
        $dataParams = $this->getData($model, $request, "loss_time_reports");
        $dataParams = $dataParams->get();
        // return Helper::getEloquentSqlWithBindings($dataParams);
        return $this->generateData(new \App\LossTimeReport , $request , "Lost Time Report".Date("YmdHis"),$dataParams );
        //  declare variable
        /* 
            $stdate = $request->stdate;
            $endate = $request->endate;
            $line   = $request->line;
            $filename = 'MOPR_LossTimeReport';

            //  execute database
            $datas  = DB::select("exec download_lost_time_report '{$stdate}', '{$endate}', '{$line}'");

            //  untuk meyimpan data di excel
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=" . $filename . ".xls");
            echo '<table border="1">';
            echo '<tr>';
            echo '<th>No</th>';
            echo '<th>Date</th>';
            echo '<th>Time Start</th>';
            echo '<th>Time End</th>';
            echo '<th>Lost [H]</th>';
            echo '<th>Line</th>';
            echo '<th>Model</th>';
            echo '<th>Prod.No</th>';
            echo '<th>N1</th>';
            echo '<th>Responsible</th>';
            echo '<th>Symptom</th>';
            echo '<th>T.Time</th>';
            echo '<th>M.P</th>';
            echo '<th>DIC</th>';
            echo '<th>Disposal</th>';
            echo '</tr>';
            $no = 1;
            for ($i = 0; $i < count($datas); $i++) {
                $data = $datas[$i];

                echo '<tr>';
                echo '<td>' . $no . '</td>';
                echo '<td>' . $data->date . '</td>';
                echo '<td>' . $data->time_start . '</td>';
                echo '<td>' . $data->time_end . '</td>';
                echo '<td>' . $data->lost . '</td>';
                echo '<td>' . $data->line . '</td>';
                echo '<td>' . $data->model . '</td>';
                echo '<td>' . $data->lotno . '</td>';
                echo '<td>' . $data->code . '</td>';
                echo '<td>' . $data->description . '</td>';
                echo '<td>' . $data->symptom . '</td>';
                echo '<td>' . $data->total_time . '</td>';
                echo '<td>' . $data->man_power . '</td>';
                echo '<td>' . $data->dic_name . '</td>';
                echo '<td>' . $data->disposal . '</td>';
                echo '</tr>';
                $no++;
            }
            echo '</table>';
        */

        
    }

    public function renderChangeModelExcel(&$spreadsheet,$data,$row = 1){
        $MASheet = $spreadsheet->getSheet(3);
        $col = 1;
        foreach ($data as $key => $value) {
            if($key == "line"){
                $explodeValue = explode(' ',$value);
                $dept = $explodeValue[0];
                $line = ltrim($explodeValue[1],'0');
                $MASheet->setCellValueByColumnAndRow($col,$row,$dept);
                $MASheet->setCellValueByColumnAndRow($col+2,$row,$line);
            }
            if($key == "report_date"){
                $MASheet->setCellValueByColumnAndRow($col+1,$row,$value);
            }
            for ($i=$col+3; $i < 12; $i++) { 
                $MASheet->setCellValueByColumnAndRow($i,$row,$value);
            }
        }
    }

    public function change_model_report(Request $request)
    {
        $this->validate($request,[
            "line" => "required",
            "start_date" => "required",
            "end_date" => "required",
        ]);
        
        $selects = [
            "change_model_reports.prev_model",
            "change_model_reports.time_start",
            "change_model_reports.next_model",
            "change_model_reports.time_end",
            "change_model_reports.total_time",
            "change_model_reports.normal_minute",
            "change_model_reports.abnormal_minute",
            "change_model_reports.major_problem",
            "change_model_reports.master_category_id"
        ];
        $datas = ChangeModelReport::selectRaw("PARSENAME(REPLACE(report_headers.line, ' ', '.'),2) AS Dept")
            ->addSelect("report_headers.report_date")
            ->selectRaw("REPLACE(LTRIM(REPLACE(PARSENAME(REPLACE(report_headers.line, ' ', '.'),1),'0',' ')),' ','0') AS Line")
            ->addSelect($selects)
            ->join('report_headers', 'report_headers.id', '=', 'change_model_reports.report_header_id')
            ->where(function ($query) use ($request){
                if ($request->has('start_date')) {
                    $query = $query->where('report_headers.report_date', '>=', $request->get('start_date'));
                }
                if ($request->has('end_date')) {
                    $query = $query->where('report_headers.report_date', '<=', $request->get('end_date'));
                }
                
                if ($request->has('month')) {
                    $params = \explode('-', $request->get('month'));
                    if(count($params) == 2) {
                        $year = $params[0];
                        $month = $params[1];
                        $query = $query->whereMonth('report_headers.report_date', $month)
                            ->whereYear('report_headers.report_date', $year);
                    }
                }
            })->whereIn('report_headers.line',$request->line)
            ->orderBy('report_headers.line', 'asc')
            ->orderBy('report_headers.report_date','asc');
        // return Helper::getEloquentSqlWithBindings($datas);
        $dataToExport = $datas->get();
        // return $dataToExport;
        $filename = 'CHANGE_MODEL_REPORT_'.Date('YmdHis');
        try {
            //code...
            return Excel::download(new DataExport($dataToExport) , "{$filename}.xls" );
        } catch (\Exception $th) {
            return [
                'success' => false,
                'message' => $th->getMessage()
            ];
        }
        
    }
    public function change_model_report_new(Request $request)
    {
        $this->validate($request,[
            "line" => "required",
            "start_date" => "required",
            "end_date" => "required",
        ]);

        ini_set('max_execution_time', '300');
        $inputFileName = "app/public/template_change_models.xls";
        // $inputFileName = "app/public/op_summary_format.xlsx";
        $template = storage_path($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
        $spreadsheet = $reader->load($template);
        
        $selects = [
            "report_headers.line",
            "report_headers.report_date",
            "change_model_reports.prev_model",
            "change_model_reports.time_start",
            "change_model_reports.next_model",
            "change_model_reports.time_end",
            "change_model_reports.total_time",
            "change_model_reports.normal_minute",
            "change_model_reports.abnormal_minute",
            "change_model_reports.major_problem",
            "change_model_reports.master_category_id"
        ];
        $datas = ChangeModelReport::select($selects)
            ->join('report_headers', 'report_headers.id', '=', 'change_model_reports.report_header_id')
            ->where(function ($query) use ($request){
                if ($request->has('start_date')) {
                    $query = $query->where('report_headers.report_date', '>=', $request->get('start_date'));
                }
                if ($request->has('end_date')) {
                    $query = $query->where('report_headers.report_date', '<=', $request->get('end_date'));
                }
                
                if ($request->has('month')) {
                    $params = \explode('-', $request->get('month'));
                    if(count($params) == 2) {
                        $year = $params[0];
                        $month = $params[1];
                        $query = $query->whereMonth('report_headers.report_date', $month)
                            ->whereYear('report_headers.report_date', $year);
                    }
                }
            })->whereIn('report_headers.line',$request->line)
            ->orderBy('report_headers.line', 'asc')
            ->orderBy('report_headers.report_date','asc');
        // return Helper::getEloquentSqlWithBindings($datas);
        $datas = $datas->get();
        // return $datas;
        // $row = 6;
        // foreach ($datas->toArray() as $data) {
        //     $this->renderChangeModelExcel($spreadsheet,$data,$row);
        //     $row++;
        // }

        $fileName = "CHANGE_MODEL_REPORT_";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->setIncludeCharts(true);
        // $writer->save($result);
        $writer->save('php://output'); 

        // return $this->downloadSpreadsheet("change_model_report{$request->start_date}", $spreadsheet);
    }
    public function change_model_report_old(Request $request)
    {
        return $this->generateData(new \App\ChangeModelReport, $request, 'change model report');

        //  declare variable
        /* $stdate = $request->stdate;
        $endate = $request->endate;
        $line   = $request->line;
        $filename = 'MOPR_ChangeModelReport';

        //  execute database
        $datas  = DB::select("exec download_change_model_report '{$stdate}', '{$endate}', '{$line}'");

        //  untuk meyimpan data di excel
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=" . $filename . ".xls");
        echo '<table border="1">';
        echo '<tr>';
        echo '<th rowspan=2>No</th>';
        echo '<th rowspan=2>Dept</th>';
        echo '<th rowspan=2>Tgl</th>';
        echo '<th rowspan=2>Line</th>';
        echo '<th rowspan=2>Prev Model (A)</th>';
        echo '<th rowspan=2>tA (Last)</th>';
        echo '<th rowspan=2>Next Model (B)</th>';
        echo '<th rowspan=2>tB (First)</th>';
        echo '<th rowspan=2>Tt</th>';
        echo '<th colspan=2>Loss Time (Menit)</th>';
        echo '<th rowspan=2>Major Problem</th>';
        echo '<th rowspan=2>Category Code</th>';
        echo '<th rowspan=2>Category Name</th>';
        echo '<th rowspan=2>PIC</th>';
        echo '</tr>';
        echo '<tr>';
        echo '<th>Normal</th>';
        echo '<th>Abnormal</th>';
        echo '</tr>';
        $no = 1;
        for ($i = 0; $i < count($datas); $i++) {
            $data = $datas[$i];

            echo '<tr>';
            echo '<td>' . $no . '</td>';
            echo '<td>' . $data->dept . '</td>';
            echo '<td>' . $data->date . '</td>';
            echo '<td>' . $data->line . '</td>';
            echo '<td>' . $data->prev_model . '</td>';
            echo '<td>' . $data->time_start . '</td>';
            echo '<td>' . $data->next_model . '</td>';
            echo '<td>' . $data->time_end . '</td>';
            echo '<td>' . $data->total_time . '</td>';
            echo '<td>' . $data->normal_minute . '</td>';
            echo '<td>' . $data->abnormal_minute . '</td>';
            echo '<td>' . $data->major_problem . '</td>';
            echo '<td>' . $data->category_code . '</td>';
            echo '<td>' . $data->category_name . '</td>';
            echo '<td>' . $data->pic . '</td>';
            echo '</tr>';
            $no++;
        }
        echo '</table>'; */
    }

    public function trial_ng_set_report(Request $request)
    {
        //  declare variable
        return $this->generateData(new \App\TrialNgSet, $request, 'trial ng sets');

        /* $stdate = $request->stdate;
        $endate = $request->endate;
        $line   = $request->line;
        $filename = 'MOPR_TrialNGSetsReport';

        //  execute database
        $datas  = DB::select("exec download_trial_ng_set_report '{$stdate}', '{$endate}', '{$line}'");

        //  untuk meyimpan data di excel
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=" . $filename . ".xls");
        echo '<table border="1">';
        echo '<tr>';
        echo '<th>No</th>';
        echo '<th>Date</th>';
        echo '<th>Line</th>';
        echo '<th>Symptom</th>';
        echo '<th>Result</th>';
        echo '<th>Shift</th>';
        echo '</tr>';
        $no = 1;
        for ($i = 0; $i < count($datas); $i++) {
            $data = $datas[$i];

            echo '<tr>';
            echo '<td>' . $no . '</td>';
            echo '<td>' . $data->date . '</td>';
            echo '<td>' . $data->line . '</td>';
            echo '<td>' . $data->symptom . '</td>';
            echo '<td>' . $data->result . '</td>';
            echo '<td>' . $data->shift . '</td>';
            echo '</tr>';
            $no++;
        }
        echo '</table>'; */
    }

    public function change_member_report(Request $request)
    {
        //  declare variable
        return $this->generateData(new \App\ChangeMemberReport, $request, 'change_member_report');

        /* $stdate = $request->stdate;
        $endate = $request->endate;
        $line   = $request->line;
        $filename = 'MOPR_ChangeMemberReport';

        //  execute database
        $datas  = DB::select("exec download_change_member_report '{$stdate}', '{$endate}', '{$line}'");

        //  untuk meyimpan data di excel
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=" . $filename . ".xls");
        echo '<table border="1">';
        echo '<tr>';
        echo '<th>No</th>';
        echo '<th>Date</th>';
        echo '<th>Line</th>';
        echo '<th>Is Change</th>';
        echo '<th>Shift</th>';
        echo '<th>Reference No</th>';
        echo '<th>Remark</th>';
        echo '</tr>';
        $no = 1;
        for ($i = 0; $i < count($datas); $i++) {
            $data = $datas[$i];

            echo '<tr>';
            echo '<td>' . $no . '</td>';
            echo '<td>' . $data->date . '</td>';
            echo '<td>' . $data->line . '</td>';
            echo '<td>' . $data->is_change . '</td>';
            echo '<td>' . $data->shift . '</td>';
            echo '<td>' . $data->refrence_no . '</td>';
            echo '<td>' . $data->remark . '</td>';
            echo '</tr>';
            $no++;
        }
        echo '</table>'; */
    }

    public function balance_sets_condition_report(Request $request)
    {
        //  declare variable
        /*  */
        $from = $request->has('start_date') ? $request->get('start_date') : Carbon::today()->firstOfMonth()
            ->format("Y-m-d");
        $to = $request->has('end_date') ? $request->get('end_date') : Carbon::today()->format("Y-m-d");

        $data = (new ProductionResult)->select([
            "production_results.*",
            "report_headers.line",
            "report_headers.report_date",
            "report_headers.schedule_date",
        ])
            ->leftJoin('report_headers', 'report_header_id', '=', 'report_headers.id')
            ->where('balance', '>', 0)
            ->whereBetween('report_headers.report_date', [ $from, $to ])
            ->where(function($query) use ($request){
                if($request->has('line')) {
                    if(is_array($request->line)){
                        $query->whereIn('report_headers.line', $request->get('line'));
                    }else{
                        $query->where('report_headers.line', $request->get('line'));
                    }
                }
            })
            ->get();

        return $this->generateData(new \App\BalanceSetCondition, $request, 'Balance Set Condition', $data );
        
        
    }

    public function production_result_reports(Request $request)
    {
        return $this->generateData(new \App\ProductionResult, $request, 'Production Result');
        
    }
    /** ---end created by---*/

    public function getAverageData(Request $request,$line) {
        # code...
        $data = ReportHeader::select([
            'report_headers.id',
            'report_headers.report_date',
            'report_headers.line',
             DB::raw('AVG( [efficiency]) as [efficiency]')
            ,DB::raw('AVG( [rate_of_operation]) as [rate_of_operation]')
            ,DB::raw('AVG( [productivity]) as [productivity]')
            ,DB::raw('AVG( [rate_of_attendance]) as [rate_of_attendance]')
            ,DB::raw('AVG( [mp_d1]) as [mp_d1]')
            ,DB::raw('AVG( [mp_d2]) as [mp_d2]')
            ,DB::raw('AVG( [working_hours]) as [working_hours]')
            ,DB::raw('AVG( [fixed_time]) as [fixed_time]')
            ,DB::raw('AVG( [absence_d1]) as [absence_d1]')
            ,DB::raw('AVG( [absence_d2]) as [absence_d2]')
            ,DB::raw('AVG( [cl_eo]) as [cl_eo]')
            ,DB::raw('AVG( [overtime]) as [overtime]')
            ,DB::raw('AVG( [assist_other_dept_plus]) as [assist_other_dept_plus]')
            ,DB::raw('AVG( [assist_other_dept_min]) as [assist_other_dept_min]')
            ,DB::raw('AVG( [assist_plus]) as [assist_plus]')
            ,DB::raw('AVG( [assist_min]) as [assist_min]')
            ,DB::raw('AVG( [tph]) as [tph]')
            ,DB::raw('AVG( [loss_time_operation_summaries_total]) as [loss_time_operation_summaries_total]')
            ,DB::raw('AVG( [indirect_op_hours_total]) as [indirect_op_hours_total]')
            ,DB::raw('AVG( [tioh]) as [tioh]')
            ,DB::raw('AVG( [dwh]) as [dwh]')
            ,DB::raw('AVG( [so]) as [so]')
            ,DB::raw('AVG( [tst]) as [tst]') //report date ada di table a, ada di table b
        ])->join('operation_summaries', 'report_headers.id', '=', 'operation_summaries.report_header_id')
        ->groupBy('report_headers.id')
        ->groupBy('report_headers.report_date')
        ->groupBy('report_headers.line')
        // ORDER BY REPORT DATE WAJIB untuk render urutan kolom header di sheet
        ->orderBy('report_headers.report_date', "asc")
        ->with([
            'actual_direct_op_hours',
            'IndirectOpHours',
            "op_summary_loss_time"
        ]);

        if ($request->has('start_date')) {
            $data = $data->where('report_headers.report_date', '>=', $request->get('start_date'));
        }
        if ($request->has('end_date')) {
            $data = $data->where('report_headers.report_date', '<=', $request->get('end_date'));
        }

        if ($request->has('month')) {
            $params = \explode('-', $request->get('month'));
            if (count($params) == 2) {
                $year = $params[0];
                $month = $params[1];
                $data = $data->whereMonth('report_headers.report_date', $month)
                    ->whereYear('report_headers.report_date', $year);
            }
        }

        if ($request->has('line')) {
            $data = $data->where('report_headers.line',   $line);
        }
        
        return $data = $data->get();
    }

    public function getAverageData_old(Request $request) {
        # code...
        $data = ReportHeader::select([
            'report_headers.id',
            'report_headers.report_date',
            'report_headers.line',
             DB::raw('AVG( [efficiency]) as [efficiency]')
            ,DB::raw('AVG( [rate_of_operation]) as [rate_of_operation]')
            ,DB::raw('AVG( [productivity]) as [productivity]')
            ,DB::raw('AVG( [rate_of_attendance]) as [rate_of_attendance]')
            ,DB::raw('AVG( [mp_d1]) as [mp_d1]')
            ,DB::raw('AVG( [mp_d2]) as [mp_d2]')
            ,DB::raw('AVG( [working_hours]) as [working_hours]')
            ,DB::raw('AVG( [fixed_time]) as [fixed_time]')
            ,DB::raw('AVG( [absence_d1]) as [absence_d1]')
            ,DB::raw('AVG( [absence_d2]) as [absence_d2]')
            ,DB::raw('AVG( [cl_eo]) as [cl_eo]')
            ,DB::raw('AVG( [overtime]) as [overtime]')
            ,DB::raw('AVG( [assist_other_dept_plus]) as [assist_other_dept_plus]')
            ,DB::raw('AVG( [assist_other_dept_min]) as [assist_other_dept_min]')
            ,DB::raw('AVG( [assist_plus]) as [assist_plus]')
            ,DB::raw('AVG( [assist_min]) as [assist_min]')
            ,DB::raw('AVG( [tph]) as [tph]')
            ,DB::raw('AVG( [loss_time_operation_summaries_total]) as [loss_time_operation_summaries_total]')
            ,DB::raw('AVG( [indirect_op_hours_total]) as [indirect_op_hours_total]')
            ,DB::raw('AVG( [tioh]) as [tioh]')
            ,DB::raw('AVG( [dwh]) as [dwh]')
            ,DB::raw('AVG( [so]) as [so]')
            ,DB::raw('AVG( [tst]) as [tst]') //report date ada di table a, ada di table b
        ])->join('operation_summaries', 'report_headers.id', '=', 'operation_summaries.report_header_id')
        ->groupBy('report_headers.id')
        ->groupBy('report_headers.report_date')
        ->groupBy('report_headers.line')
        ->with([
            'actual_direct_op_hours',
            'IndirectOpHours',
            "op_summary_loss_time"
        ]);

        if ($request->has('start_date')) {
            $data = $data->where('report_headers.report_date', '>=', $request->get('start_date'));
        }
        if ($request->has('end_date')) {
            $data = $data->where('report_headers.report_date', '<=', $request->get('end_date'));
        }

        if ($request->has('month')) {
            $params = \explode('-', $request->get('month'));
            if (count($params) == 2) {
                $year = $params[0];
                $month = $params[1];
                $data = $data->whereMonth('report_headers.report_date', $month)
                    ->whereYear('report_headers.report_date', $year);
            }
        }

        if ($request->has('line')) {
            $data = $data->where('report_headers.line',   $request->get('line'));
        }

        return $data = $data->get();
    }

    public function renderOpSummaryByLine($request, $line,$spreadsheet){
        // check group untuk membagi report per group
        $groups = OperationSummary::select(['group'])
        ->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
        ->where(function($query) use ($request,$line) {
            
            // $from = $request->has('start_date') ? 
            //     $request->get('start_date') : Carbon::today()->firstOfMonth()->format('Y-m-d');

            // $to = $request->has('end_date') ? 
            //     $request->get('end_date') : Carbon::today()->format('Y-m-d');

            // $query->whereBetween('report_headers.report_date', [$from, $to]);

            if ($request->has('start_date')) {
                $query = $query->where('report_headers.report_date', '>=', $request->get('start_date'));
            }
            if ($request->has('end_date')) {
                $query = $query->where('report_headers.report_date', '<=', $request->get('end_date'));
            }
            
            if ($request->has('month')) {
                $params = \explode('-', $request->get('month'));
                if(count($params) == 2) {
                    $year = $params[0];
                    $month = $params[1];
                    $query = $query->whereMonth('report_headers.report_date', $month)
                        ->whereYear('report_headers.report_date', $year);
                }
            }
            $query->where('report_headers.line', $line);
            

        })->distinct('group')
        ->get();
        // return Helper::getEloquentSqlWithBindings($groups);
        if(count($groups) == 0){
            return 'next';
        }
        
        $res = [];
        foreach ($groups as $key => $group) {
            $group = $group['group'];
            # code...
            $data = OperationSummary::select([
                'operation_summaries.*', //report date ada di table a, ada di table b
                'report_headers.report_date',
                'report_headers.line',
                'report_headers.created_by'
            ])->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
                ->where('group', $group)
                ->with([
                    'actual_direct_op_hours',
                    'IndirectOpHours',
                    "op_summary_loss_time"
                ]);
                    
            if ($request->has('start_date')) {
                $data = $data->where('report_headers.report_date', '>=', $request->get('start_date'));
            }
            if ($request->has('end_date')) {
                $data = $data->where('report_headers.report_date', '<=', $request->get('end_date'));
            }
            
            if ($request->has('month')) {
                $params = \explode('-', $request->get('month'));
                if(count($params) == 2) {
                    $year = $params[0];
                    $month = $params[1];
                    $data = $data->whereMonth('report_headers.report_date', $month)
                        ->whereYear('report_headers.report_date', $year);
                }
            }

            $data = $data->where('report_headers.line', $line);

            // ORDER BY REPORT DATE WAJIB untuk render urutan kolom header di sheet
            $data = $data->orderBy('report_headers.report_date',"asc");

            $data = $data->get();
            // return $data;
            // $result = collect($request->all());
            // return $result->merge(['data'=>$data]);
            if(count($data) == 0) {
                // kalo data kosong, kita skip aja. gausah render;
                continue;
            }
            
            $sheet_name = Helper::generateSheetNameGroup($line, $group);
            $res[$sheet_name] = $this->renderSheet( $sheet_name, $spreadsheet, $data );

            
            // $res["group {$group}"] = $this->renderSheet( $sheet_name, $spreadsheet, $data );
            // $res["group {$group}"] = $this->getFormat('app/public/op_summary_format.xlsx', $data);
        }
        
        /**
         * fungsi ini tidak digunakan lagi, karena tidak diperlukan di by syam.
         * seharusnya adalah summary data bukan average data. ada miscommunication.
         */
        /* kalau group nya lebih dari 1, kita generate juga data rata-ratanya */
        // if(count($groups) > 1) {
        //     $avg = $this->getAverageData($request,$line);
        //     $avg_name = Helper::generateLineForSheetName($line);
        //     $res["{$avg_name}-AVG"] = $this->renderSheet("{$avg_name}-AVG", $spreadsheet , $avg);

        //     // return $res;
        // }
        $lines[$line] = $res; 

        return $lines;
    }

    public function operation_summary_reports(Request $request)
    {
        ini_set('max_execution_time', '300');
        // return $request->all();
        /*
            - operation summary harus render 3 sheet per line;
                - gabung pake average based on report_header_id, line, 
                - group 1, group 2, group 3 ( jika ada )
            solution:
            - get data collection average;
            - get data collection first group;
            - get data collection second group;

            loop through the data;
            feed to getFormat;

        */

        /* 
            dalam data ini, ada berapa group;
        */
        $this->validate($request, [
            "line" => "required",
            "month" => "required",
        ]);
        // return $request->get('line');
        
        $inputFileName = 'app/public/op_summary_format.xlsx';
        $template = storage_path($inputFileName);
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template);
        // ==============================================
        if($request->has('line')) {
            // =================================== jika diterima line array ==========================
            if(is_array($request->line)){
                $lines = [];
                foreach ($request->line as $line) {
                    $lines[$line] = $this->renderOpSummaryByLine($request,$line,$spreadsheet);
                    if($lines[$line] == 'next'){
                        continue;
                    }
                }
                // return $lines;
            }
            
            // =================================== jika diterima line bukan array ====================
            else{
                $one_line = $this->renderOpSummaryByLine($request,$request->get('line'),$spreadsheet);
            }
        }
        // ==============================================
        // return $spreadsheet;
        // return $one_line;
        // return $lines;
        
        return $this->downloadSpreadsheet("operation_summary_report {$request->month}", $spreadsheet);

        // // return $data;
        // return $this->getFormat('app/public/op_summary_format.xlsx', $data);
    }
    public function operation_summary_reports_array_dan_bukan_dipisah(Request $request)
    {
        ini_set('max_execution_time', '300');
        // return $request->all();
        /*
            - operation summary harus render 3 sheet per line;
                - gabung pake average based on report_header_id, line, 
                - group 1, group 2, group 3 ( jika ada )
            solution:
            - get data collection average;
            - get data collection first group;
            - get data collection second group;

            loop through the data;
            feed to getFormat;

        */

        /* 
            dalam data ini, ada berapa group;
        */
        $this->validate($request, [
            "line" => "required",
            "month" => "required",
        ]);
        // return $request->get('line');
        $lines = [];
        
        $inputFileName = 'app/public/op_summary_format.xlsx';
        $template = storage_path($inputFileName);
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template);
        // ==============================================
        if($request->has('line')) {
            // =================================== jika diterima line array ==========================
            if(is_array($request->line)){
                foreach ($request->line as $line) {
                    // check group untuk membagi report per group
                    $groups = OperationSummary::select(['group'])
                    ->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
                    ->where(function($query) use ($request,$line) {
                        
                        // $from = $request->has('start_date') ? 
                        //     $request->get('start_date') : Carbon::today()->firstOfMonth()->format('Y-m-d');
        
                        // $to = $request->has('end_date') ? 
                        //     $request->get('end_date') : Carbon::today()->format('Y-m-d');
        
                        // $query->whereBetween('report_headers.report_date', [$from, $to]);

                        if ($request->has('start_date')) {
                            $query = $query->where('report_headers.report_date', '>=', $request->get('start_date'));
                        }
                        if ($request->has('end_date')) {
                            $query = $query->where('report_headers.report_date', '<=', $request->get('end_date'));
                        }
                        
                        if ($request->has('month')) {
                            $params = \explode('-', $request->get('month'));
                            if(count($params) == 2) {
                                $year = $params[0];
                                $month = $params[1];
                                $query = $query->whereMonth('report_headers.report_date', $month)
                                    ->whereYear('report_headers.report_date', $year);
                            }
                        }
                        $query->where('report_headers.line', $line);
                        
        
                    })->distinct('group')
                    ->get();
                    // return Helper::getEloquentSqlWithBindings($groups);
                    if(count($groups) == 0){
                        continue;
                    }
                    
                    $res = [];
                    foreach ($groups as $key => $group) {
                        $group = $group['group'];
                        # code...
                        $data = OperationSummary::select([
                            'operation_summaries.*', //report date ada di table a, ada di table b
                            'report_headers.report_date',
                            'report_headers.line',
                            'report_headers.created_by'
                        ])->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
                            ->where('group', $group)
                            ->with([
                                'actual_direct_op_hours',
                                'IndirectOpHours',
                                "op_summary_loss_time"
                            ]);
                                
                        if ($request->has('start_date')) {
                            $data = $data->where('report_headers.report_date', '>=', $request->get('start_date'));
                        }
                        if ($request->has('end_date')) {
                            $data = $data->where('report_headers.report_date', '<=', $request->get('end_date'));
                        }
                        
                        if ($request->has('month')) {
                            $params = \explode('-', $request->get('month'));
                            if(count($params) == 2) {
                                $year = $params[0];
                                $month = $params[1];
                                $data = $data->whereMonth('report_headers.report_date', $month)
                                    ->whereYear('report_headers.report_date', $year);
                            }
                        }
            
                        $data = $data->where('report_headers.line', $line);

                        // ORDER BY REPORT DATE WAJIB untuk render urutan kolom header di sheet
                        $data = $data->orderBy('report_headers.report_date',"asc");
            
                        $data = $data->get();
                        // return $data;
                        // $result = collect($request->all());
                        // return $result->merge(['data'=>$data]);
                        if(count($data) == 0) {
                            // kalo data kosong, kita skip aja. gausah render;
                            continue;
                        }
                        
                        $sheet_name = Helper::generateSheetNameGroup($line, $group);
                        $res[$sheet_name] = $this->renderSheet( $sheet_name, $spreadsheet, $data );

                        
                        // $res["group {$group}"] = $this->renderSheet( $sheet_name, $spreadsheet, $data );
                        // $res["group {$group}"] = $this->getFormat('app/public/op_summary_format.xlsx', $data);
                    }
                    
                    /**
                     * fungsi ini tidak digunakan lagi, karena tidak diperlukan di by syam.
                     * seharusnya adalah summary data bukan average data. ada miscommunication.
                     */
                    /* kalau group nya lebih dari 1, kita generate juga data rata-ratanya */
                    // if(count($groups) > 1) {
                    //     $avg = $this->getAverageData($request,$line);
                    //     $avg_name = Helper::generateLineForSheetName($line);
                    //     $res["{$avg_name}-AVG"] = $this->renderSheet("{$avg_name}-AVG", $spreadsheet , $avg);

                    //     // return $res;
                    // }
                    $lines[$line] = $res; 
                }
                // return $lines;
            }
            
            // =================================== jika diterima line bukan array ====================
            else{
                $groups = OperationSummary::select(['group'])
                    ->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
                    ->where(function($query) use ($request) {
                    // $from = $request->has('start_date') ? 
                    //     $request->get('start_date') : Carbon::today()->firstOfMonth()->format('Y-m-d');
    
                    // $to = $request->has('end_date') ? 
                    //     $request->get('end_date') : Carbon::today()->format('Y-m-d');
    
                    // $query->whereBetween('report_headers.report_date', [$from, $to]);

                    if ($request->has('start_date')) {
                        $query = $query->where('report_headers.report_date', '>=', $request->get('start_date'));
                    }
                    if ($request->has('end_date')) {
                        $query = $query->where('report_headers.report_date', '<=', $request->get('end_date'));
                    }
                    
                    if ($request->has('month')) {
                        $params = \explode('-', $request->get('month'));
                        if(count($params) == 2) {
                            $year = $params[0];
                            $month = $params[1];
                            $query = $query->whereMonth('report_headers.report_date', $month)
                                ->whereYear('report_headers.report_date', $year);
                        }
                    }
                    if($request->has('line')) {
                        $query->where('report_headers.line', $request->get('line'));
                    }
    
                })->distinct('group')
                ->get();
                // return Helper::getEloquentSqlWithBindings($groups);
                // return $groups;
                if(count($groups) == 0){
                   return "no data";
                }
                $res = [];
                foreach ($groups as $key => $group) {
                    $group = $group['group'];
                    # code...
                    $data = OperationSummary::select([
                        'operation_summaries.*', //report date ada di table a, ada di table b
                        'report_headers.report_date',
                        'report_headers.line',
                        'report_headers.created_by'
                    ])->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
                        ->where('group', $group)
                        ->with([
                            'actual_direct_op_hours',
                            'IndirectOpHours',
                            "op_summary_loss_time"
                        ]);
        
                    if ($request->has('start_date')) {
                        $data = $data->where('report_headers.report_date', '>=', $request->get('start_date'));
                    }
                    if ($request->has('end_date')) {
                        $data = $data->where('report_headers.report_date', '<=', $request->get('end_date'));
                    }
                    
                    if ($request->has('month')) {
                        $params = \explode('-', $request->get('month'));
                        if(count($params) == 2) {
                            $year = $params[0];
                            $month = $params[1];
                            $data = $data->whereMonth('report_headers.report_date', $month)
                                ->whereYear('report_headers.report_date', $year);
                        }
                    }
        
                    if ($request->has('line')) {
                        $data = $data->where('report_headers.line',   $request->get('line'));
                    }
        
                    $data = $data->get();
        
                    // $result = collect($request->all());
                    // return $result->merge(['data'=>$data]);
                    if(count($data) == 0) {
                        // kalo data kosong, kita skip aja. gausah render;
                        continue;
                    }
                    
                    $sheet_name = Helper::generateSheetNameGroup($request->get('line'), $group);
                    $res[$sheet_name] = $this->renderSheet( $sheet_name, $spreadsheet, $data );
                    // $res["group {$group}"] = $this->getFormat('app/public/op_summary_format.xlsx', $data);
                }
        
                /**
                 * fungsi ini tidak digunakan lagi, karena tidak diperlukan di by syam.
                 * seharusnya adalah summary data bukan average data. ada miscommunication.
                 */
                /* kalau group nya lebih dari 1, kita generate juga data rata-ratanya */
                // if(count($groups) > 1) {
                //     //$avg = $this->getAverageData($request);
                //     $avg = $this->getAverageData($request,$request->get('line'));
                //     $avg_name = Helper::generateLineForSheetName($request->get('line'));
                //     $res["{$avg_name}-AVG"] = $this->renderSheet("{$avg_name}-AVG", $spreadsheet , $avg);
                //     // $res["avg"] = $this->renderSheet("average {$request->get('line')}", $spreadsheet , $avg);
                // }
                $lines[$request->get('line')] = $res;
            }
        }
        // ==============================================
        // return $spreadsheet;
        // return $lines;
        
        return $this->downloadSpreadsheet("operation_summary_report {$request->month}", $spreadsheet);

        // // return $data;
        // return $this->getFormat('app/public/op_summary_format.xlsx', $data);
    }
    public function operation_summary_reports_old(Request $request)
    {
        // return $request->all();
        /*
            - operation summary harus render 3 sheet per line;
                - gabung pake average based on report_header_id, line, 
                - group 1, group 2, group 3 ( jika ada )
            solution:
            - get data collection average;
            - get data collection first group;
            - get data collection second group;

            loop through the data;
            feed to getFormat;

        */

        /* 
            dalam data ini, ada berapa group;
        */
        $this->validate($request, [
            "line" => "required",
            "month" => "required",
        ]);

        $groups = OperationSummary::select(['group'])
            ->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
            ->where(function($query) use ($request) {
                $from = $request->has('start_date') ? 
                    $request->get('start_date') : Carbon::today()->firstOfMonth()->format('Y-m-d');

                $to = $request->has('end_date') ? 
                    $request->get('end_date') : Carbon::today()->format('Y-m-d');

                $query->whereBetween('report_headers.report_date', [$from, $to]);
                if($request->has('line')) {
                    $query->where('report_headers.line', $request->line);
                }

            })->distinct('group')
            ->get();
        // return Helper::getEloquentSqlWithBindings($groups);

        $inputFileName = 'app/public/op_summary_format.xlsx';
        $template = storage_path($inputFileName);
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template);
        
        $res = [];
        foreach ($groups as $key => $group) {
            $group = $group['group'];
            # code...
            $data = OperationSummary::select([
                'operation_summaries.*', //report date ada di table a, ada di table b
                'report_headers.report_date',
                'report_headers.line',
                'report_headers.created_by'
            ])->join('report_headers', 'report_headers.id', '=', 'operation_summaries.report_header_id')
                ->where('group', $group)
                ->with([
                    'actual_direct_op_hours',
                    'IndirectOpHours',
                    "op_summary_loss_time"
                ]);

            if ($request->has('start_date')) {
                $data = $data->where('report_headers.report_date', '>=', $request->get('start_date'));
            }
            if ($request->has('end_date')) {
                $data = $data->where('report_headers.report_date', '<=', $request->get('end_date'));
            }
            
            if ($request->has('month')) {
                $params = \explode('-', $request->get('month'));
                if(count($params) == 2) {
                    $year = $params[0];
                    $month = $params[1];
                    $data = $data->whereMonth('report_headers.report_date', $month)
                        ->whereYear('report_headers.report_date', $year);
                }
            }

            if ($request->has('line')) {
                $data = $data->where('report_headers.line',   $request->get('line'));
            }

            $data = $data->get();

            // $result = collect($request->all());
            // return $result->merge(['data'=>$data]);
            if(count($data) == 0) {
                // kalo data kosong, kita skip aja. gausah render;
                continue;
            }


            $res["group {$group}"] = $this->renderSheet( "{$request->line}-{$group}", $spreadsheet, $data );
            // $res["group {$group}"] = $this->getFormat('app/public/op_summary_format.xlsx', $data);
        }

        /* kalau group nya lebih dari 1, kita generate juga data rata-ratanya */
        if(count($groups) > 1) {
            $avg = $this->getAverageData_old($request);
            $res['avg'] = $this->renderSheet("average", $spreadsheet , $avg);
        }
        
        // return $res;
        return $this->downloadSpreadsheet("operation_summary_report {$request->line} {$request->month}", $spreadsheet);

        // // return $data;
        // return $this->getFormat('app/public/op_summary_format.xlsx', $data);
    }

    public function getDiffDates($start, $end){
        $res = Carbon::createFromFormat('Y-m-d', $start)->diffInDays($end);//->diffInDays($end);

        if($start != $end){
            $res++;
        }

        return $res;
        // return compact('start', 'end', 'res');

    }

    public function renderTanggal(Worksheet &$sheet, $data) {
        if(count($data) > 0) {
            if(!isset($data[0]['report_date'])){
                return;
            }

            $reportDate = $data[0]['report_date'];
            
            $firstOfMonth = Carbon::createFromFormat('Y-m-d', $reportDate)
                ->firstOfMonth()
                ->format('Y-m-d');
            
            $daysCount = Carbon::createFromFormat('Y-m-d', $reportDate)
                ->daysInMonth;
            
            $indexFirstDay = $this->getIndexFirstDay($firstOfMonth);
            
            $threshold = $this->threshold;
            $row = $this->getCol('report_date');
            $dayIndex = 0;
            $max = ($indexFirstDay + $this->colCount) + $daysCount;
            $res = [];
            for ($colCount = $indexFirstDay + $this->colCount ; $colCount < $max; $colCount++) {
                $day = Carbon::createFromFormat('Y-m-d', $firstOfMonth );
                # code...
                if ($colCount >= $threshold) {
                    /* 
                        yg harus di skip:
                        M, U, AC, AK, AS, BA
                        13, 21, 29, 37, 45,
                        6+7, 13+1+7, 21+1+7, 
                    */
                    // supaya skip nya ga ada yang kelewat.
                    while ($colCount >= $threshold) {
                        # code...
                        $colCount++;
                        $max++; //add max;
                        // $dayIndex++;
                        if ($colCount > $threshold) {
                            $threshold += 8;
                        }
                    }
                }
                $value = $day->addDay($dayIndex)->format('d-M');
                $sheet->setCellValueByColumnAndRow($colCount, $row, $value);
                $res[] = compact('value', 'dayIndex','colCount', 'row', 'daysCount', 'max');
                $dayIndex++;
            }

            return $res;
        }
    }

    public function renderSheet($titleSheet, &$spreadsheet, $data) {
        // return '';
        $firstSheet = $spreadsheet->getActiveSheet();
        $sheet = clone $firstSheet;
        $sheet->setTitle($titleSheet);
        $spreadsheet->addSheet($sheet);

        $fileName = 'operation_summary';

        if (\count($data) > 0) {
            if (isset($data[0]) && isset($data[0]['report_date'])) {
                $this->renderTanggal($sheet, $data);
            }
        }

        /* 
            // we need to make colCount from method due to we need to render on 
            // every weekend. 
            // contoh : jika jumat, kita harus mulai render dari col 6 + 4 = 10; dengan 
            // asumsi bahwa senin = 0 and minggu = 6;
        */

        $colCount = $this->colCount; //F mulai dari index 1;
        $firstColCount = $this->colCount;
        $rowCount = 6; // row 6 dari row pertama;
        $res = [];
        // render data;
        $temp = [];
        foreach ($data as $rowKey => $row) {
            // $sheet->getCell()->setValue();
            if ($rowKey == 0) {
                /* 
                    ada aturan baru, semua tanggal dari tanggal satu harus di render.
                    jadi kita harus :
                    - cari tau tanggal satu itu hari apa.
                    - check firstDay index berapa
                    - tambah tanggal berapa data pertama.
                    contoh:
                    - tgl 2020-08-01 hari sabtu.
                    - hari sabtu di render di col ke 6+6 = 12;
                    - data pertama tanggal 13. jadi 12 + 13; = 25
                    
                */
                $firstOfMonth = Carbon::createFromFormat('Y-m-d', $row->report_date)->firstOfMonth()->format('Y-m-d');
                // $diff = $this->getDiffDates($firstOfMonth, '2020-08-03');
                $indexFirstDay = $this->getIndexFirstDay($firstOfMonth);
                // $diffDate = $this->getDiffDates($firstOfMonth, $row->report_date);
                $colCount = $colCount + $indexFirstDay ;
                $threshold = $this->threshold;
                
                $tmp =  \compact(
                    'firstOfMonth',
                    'indexFirstDay',
                    'diffDate',
                    'colCount',
                    'threshold'
                );

                // render line & created_at
                $sheet->setCellValue("E3", $row->created_by);
                $sheet->setCellValue("E4", $row->line);
            }

            // here lies when you want changes the row data;
            $reportDate = $row->report_date;
            $row->report_date = Carbon::createFromFormat('Y-m-d', $row->report_date )->format('d-M');
            // kalau senin dan bukan iterasi pertama, skip 1;
            
            // get col index based on report_date berbanding tanggal di header;
            $currentHeaderDate = $sheet->getCellByColumnAndRow($colCount, $this->getCol('report_date') );

            if( \is_object($currentHeaderDate) ){
                $currentHeaderDate = $currentHeaderDate->getValue();
            }

            // $temp[] = $currentHeaderDate;
            if(strtoupper($currentHeaderDate) == "TOTAL"){
                $currentHeaderDate = $reportDate;
            }
            elseif($currentHeaderDate != null) {
                $year = \explode('-', $reportDate)[0];
                $currentHeaderDate = $currentHeaderDate . "-". $year;
                // hati hati saat bikin carbon tanpa data yg lengkap.
                // tahunnya pasti akan ambil tahun berjalan. padahal kebutuhan kita belum tentu seperti itu.
                // try {
                    //code...
                    $currentHeaderDate = Carbon::createFromFormat("d-M-Y", $currentHeaderDate)->format('Y-m-d');
                // } catch (\Exception $th) {
                //     //throw $th;
                //     return $temp;
                // }
            }
            else {
                $currentHeaderDate = $reportDate;
            }

            $diffDate = $this->getDiffDates($currentHeaderDate, $reportDate);
            $colCount += $diffDate;
            
            if ($colCount >= $threshold) {
                /* 
                    yg harus di skip:
                    M, U, AC, AK, AS, BA
                    13, 21, 29, 37, 45,
                    6+7, 13+1+7, 21+1+7, 
                */
                // supaya skip nya ga ada yang kelewat.
                while ($colCount >= $threshold) {
                    # code...
                    $colCount++;
                    if ($colCount > $threshold) {
                        $threshold += 8;
                    }
                }
            }
            // return compact('tmp',  'diffDate', 'colCount');
            foreach ($row->toArray() as $colKey => $colValue) {
                $rowCount = $this->getCol($colKey);

                // disini ada juga yang isinya array, handle :
                if (is_array($colValue)) {
                    if ($colKey == 'op_summary_loss_time') {
                        $renderOpSummary = $this->renderOpSummaryLossTime($sheet, $colValue, $colCount);
                    }

                    if ($colKey == 'indirect_op_hours') {
                        $renderIndirect = $this->renderIndirectOpHours($sheet, $colValue, $colCount);
                    }

                    if ($colKey == 'actual_direct_op_hours') {
                        $this->renderActualDiirectOpHours($sheet, $colValue, $colCount, $row['tst']);
                    }
                }

                if (is_null($rowCount)) {
                    continue;
                }
                
                if($colKey == "assist_min"){
                    $colValue = $row->assist_plus - $colValue;
                    $colValue = -$colValue;
                    $sheet->setCellValueByColumnAndRow($colCount, $rowCount, $colValue);
                }
                else{
                    $sheet->setCellValueByColumnAndRow($colCount, $rowCount, $colValue);
                }

                $res[$row->report_date][] = compact(
                    'colCount',
                    'rowCount',
                    'colValue',
                    'colKey',
                    'diffDate',
                    'currentHeaderDate',
                    'reportDate'
                );
            }
            $colCount++;
        }

        // $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        // $writer->setIncludeCharts(true);
        // $writer->save('php://output');

        return $res;//$spreadsheet;
    }

    public function downloadSpreadsheet($fileName, $spreadsheet){
        // return $spreadsheet;
        // delete first sheet
        if($spreadsheet->getSheetCount() > 1) {
            $spreadsheet->removeSheetByIndex(0);
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->setIncludeCharts(true);
        // $writer->save($result);
        $writer->save('php://output');

        
    }

    public function getOpSummaryLossTimeSetting()
    {
        return setting('admin.op_summary_loss_time_columns', [
            "BOARD DELAY" => 19,
            "PART DELAY" => 20,
            "QLTY PROBLEM" => 21,
            "REWORK/SELECT" => 22,
            "TRANS PROBLEM" => 23,
            "EQP TROUBLE" => 24,
            "OTHERS" => 25,
            "NO SCHEDULE" => 26,
            "EXTRA MEMBER" => 27,
        ]);
    }

    protected $rows = [
        // "id" => 2,
        // "efficiency" => 53, //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "rate_of_operation" => 54, //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "productivity" => 55, //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "rate_of_attendance" => 56, //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        "mp_d1" => 7,
        "mp_d2" => 8,
        "working_hours" => 9,
        // "fixed_time" => 100,
        "absence_d1" => 11,
        "absence_d2" => 82, // diaktifkan di row 82 permintaan bu syam tgl 14 Apr 2021
        "cl_eo" => 12,
        "overtime" => 13,
        "assist_other_dept_plus" => 14,
        "assist_other_dept_min" => 15,
        // "assist_plus" => 16,  //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        "assist_min" => 17,
        // "tph" => 18, //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "created_at" => "2020-06-08 05:33:52.230",
        // "updated_at" => "2020-06-16 08:09:02.190",
        // "loss_time_operation_summaries_total" => 28,  //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "indirect_op_hours_total" => 36,  //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "tioh" => 37,  //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "dwh" => 38,  //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        "so" => 51,
        // "tst" => 52,  //tidak perlu karena sudah ada formula dari excel request bu syam tgl 14 Apr 2021
        // "shift" => "A",
        "report_date" => 6,
        // "report_header_id" => 1,
        // "group" => null,
        // "line" => "MA 1",
    ];

    public function getCol($key)
    {
        return (isset($this->rows[$key])) ? $this->rows[$key] : null;
    }

    public function renderOpSummaryLossTimeTest($datas, $colCount = 5)
    {
        $setting = $this->getOpSummaryLossTimeSetting();
        // render it here;
        $results = [];
        foreach ($datas as $key => $value) {
            // loss code name wajib sama persis dengan pengaturan
            $lossCodeName = strtoupper(substr($value['loss_code_name'], 3));

            // substr akan return false kalau index > dari string.length;
            $results[] = [
                'lossCodeName' => $lossCodeName,
                'isset' => isset($setting[$lossCodeName]),
                'row' => $row = (isset($setting[$lossCodeName])) ? $setting[$lossCodeName] : null,
                'render' => ['col' =>  $colCount, 'row' => $row, 'value' => $value['total']]
            ];

            if (!$lossCodeName) {

                if (isset($setting[$lossCodeName])) {
                    $row = $setting[$lossCodeName];

                    // $sheet->setCellValueByColumnAndRow($colCount, $row, $value['total']);
                }
            }
        }
        return $results;
    }

    public function renderOpSummaryLossTime(Worksheet &$sheet, $datas, $colCount)
    {
        $setting = $this->getOpSummaryLossTimeSetting();
        // render it here;
        $total = [];
        foreach ($datas as $key => $value) {
            // loss code name wajib sama persis dengan pengaturan
            $lossCodeName = strtoupper(substr($value['loss_code_name'], 3));

            // substr akan return false kalau index > dari string.length;
            if ($lossCodeName != false) {
                if (isset($setting[$lossCodeName])) {
                    $row = $setting[$lossCodeName];
                    $total[$lossCodeName] = (isset($total[$lossCodeName])) ? $total[$lossCodeName] + $value['total'] : $value['total'];
                    $sheet->setCellValueByColumnAndRow($colCount, $row, $total[$lossCodeName]);
                }
            }
        }
    }

    public function getIndirectOpHoursSetting()
    {
        return setting('admin.indirect_op_hours_columns', [
            "ANNUAL LEAVE" => 29,
            "MEETING" => 30,
            "MEDICAL LEAVE" => 31,
            "MEDICAL CONS." => 32,
            "CHANGE MODEL (N)" => 33,
            "CHANGE MODEL (A)" => 34,
            "INVENTORY" => 35,
        ]);
    }

    public function renderIndirectOpHours(&$sheet, $datas, $colCount)
    {
        // render it here;
        $setting = $this->getIndirectOpHoursSetting();

        foreach ($datas as $i => $value) {
            # code...
            $key = \strtoupper($value['type']);
            if (isset($setting[$key])) {
                $row = $setting[$key];
                $sheet->setCellValueByColumnAndRow($colCount, $row, $value['value']);
            }
        }
    }

    protected $uniqueModelRow = [];
    public function renderActualDiirectOpHours(Worksheet &$sheet, $datas, $colCount, $tst = 0)
    {
        // render it here;
        $firstRow = 39;
        // return $datas;
        $sum_qty = 0;
        $sum_tst = 0;
        foreach ($datas as $key => $value) {
            $sum_qty += $value['qty']; 
            $sum_tst += $value['total']; 
        }
        // return [
        //     "qty" => $sum_qty,
        //     "tst" => number_format($sum_tst, 3, '.', '')
        // ];
        $sheet->setCellValueByColumnAndRow($colCount, $firstRow, $sum_qty);
        $sheet->setCellValueByColumnAndRow($colCount, $firstRow + 1, number_format($sum_tst, 3, '.', ''));
    }
    public function renderActualDiirectOpHours_old(Worksheet &$sheet, $datas, $colCount, $tst = 0)
    {
        // render it here;
        $firstRow = 39;
        // return $datas;
        foreach ($datas as $key => $value) {
            if (count($this->uniqueModelRow) == 0) {
                $this->uniqueModelRow[$value['model']] = $firstRow;
            } else {
                if (!isset($this->uniqueModelRow[$value['model']])) {
                    $this->uniqueModelRow[$value['model']] = $firstRow + count($this->uniqueModelRow) * 2;
                }
            }
            // row model
            $sheet->setCellValueByColumnAndRow(3, $this->uniqueModelRow[$value['model']], $value['model']);
            $sheet->setCellValueByColumnAndRow($colCount, $this->uniqueModelRow[$value['model']], $value['qty']);
            $sheet->setCellValueByColumnAndRow($colCount, $this->uniqueModelRow[$value['model']] + 1, $value['total']);
        }
    }

    public function getIndexFirstDay($date) {
        /* 
            senin = 0;
            minggu = 6;
            begitu seterusnya;
        */

        $days = [
            "Monday"    => 0,
            "Tuesday"   => 1,
            "Wednesday" => 2,
            "Thursday"  => 3,
            "Friday"    => 4,
            "Saturday"  => 5,
            "Sunday"    => 6,
        ];

        $carbon = Carbon::createFromFormat('Y-m-d', $date );

        $dayName = $carbon->format('l');
        
        return $days[$dayName];
    }

    public function getFormat($inputFileName, $data = [])
    {
        $template = storage_path($inputFileName);
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template);
        $sheet = $spreadsheet->getActiveSheet();

        $fileName = 'operation_summary';

        /* 
            // we need to make colCount from method due to we need to render on 
            // every weekend. 
            // contoh : jika jumat, kita harus mulai render dari col 6 + 4 = 10; dengan 
            // asumsi bahwa senin = 0 and minggu = 6;
        */

        $colCount = 6; //F mulai dari index 1;
        $rowCount = 6; // row 6 dari row pertama;
        $res = [];
        // render data;
        foreach ($data as $rowKey => $row) {
            // $sheet->getCell()->setValue();
            if($rowKey == 0) {
                $colCount = $colCount + $this->getIndexFirstDay($row->report_date);
                $threshold = 13;
            }

            // kalau senin dan bukan iterasi pertama, skip 1;
            if( $colCount == $threshold ) {
                /* 
                    yg harus di skip:
                    M, U, AC, AK, AS, BA
                    13, 21, 29, 37, 45,
                    6+7, 13+1+7, 21+1+7, 
                */
                $colCount++;
                if($colCount > $threshold) {
                    $threshold += 8;
                }
            }

            foreach ($row->toArray() as $colKey => $colValue) {
                $rowCount = $this->getCol($colKey);

                // disini ada juga yang isinya array, handle :
                if (is_array($colValue)) {
                    if ($colKey == 'op_summary_loss_time') {
                        $renderOpSummary = $this->renderOpSummaryLossTime($sheet, $colValue, $colCount);
                    }

                    if ($colKey == 'indirect_op_hours') {
                        $renderIndirect = $this->renderIndirectOpHours($sheet, $colValue, $colCount);
                    }

                    if ($colKey == 'actual_direct_op_hours') {
                        $this->renderActualDiirectOpHours($sheet, $colValue, $colCount, $row['tst']);
                    }
                }

                if (is_null($rowCount)) {
                    continue;
                }
                $sheet->setCellValueByColumnAndRow($colCount, $rowCount, $colValue);
                $res[] = compact(
                    'colCount',
                    'rowCount',
                    'colValue',
                    'colKey'
                );
            }
            $colCount++;
        }
        // return $res;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->setIncludeCharts(true);
        // $writer->save($result);
        $writer->save('php://output');
    }
}