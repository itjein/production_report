<?php

namespace App\Http\Controllers;

use App\IndirectOpHour;
use App\OperationSummary;
use Illuminate\Http\Request;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Log;
use App\LossTimeReport;
use App\ProductionResult;

class TestController extends Controller
{
    //
    public function test(Request $request)
    {
        $c = new ReportController;
        return $c->getAverageData($request);

    }

    public function getData()
    {
        $data = OperationSummary::find(52);


        $data->initComputedValue();

        return $data;
    }
}
