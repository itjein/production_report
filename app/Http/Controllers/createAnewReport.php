<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReportsHeader;
use App\Line;
use App\Approval;
class createAnewReport extends Controller
{
    public function index(){
        //panggil yang ada di database
        $data = ReportsHeader::orderBy('id','desc')->get();
        $dropdown = Line::get();
        $no_approval = rand();
        //return view
        return view('admin.vendor.newreport.index',compact('data','dropdown','no_approval'));
    }

    public function create(Request $request){
        // return $request;
        // sebagai tes inputan di form

        ReportsHeader::create($request->all());

        return redirect('/admin/report_header');
    }

    public function destroy($id){
        // return "Route berhasil";
        // sebagai tes route apakah berhasil atau tidak ke controller
        ReportsHeader::where('id',$id)->delete();

        return redirect('/admin/report_header');
    }

    public function view($id){
        $edit = ReportsHeader::findOrFail($id);

        return view('admin.vendor.newreport.view',compact('edit'));

    }
}
