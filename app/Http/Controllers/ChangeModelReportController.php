<?php

namespace App\Http\Controllers;

use App\ChangeModelReport;
use Illuminate\Http\Request;


class ChangeModelReportController extends CrudController
{
    protected $model = "App\\ChangeModelReport";
}
