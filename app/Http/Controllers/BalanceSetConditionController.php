<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductionResult;
use App\ReportHeader;
use App\Helper\Helper;

class BalanceSetConditionController extends CrudController
{
    protected $model = 'App\\ProductionResult';

    public function index(Request $request)
    {
        // ambil data production results yg balance nya > 0;
        // yang harinya kurang dari hari ini.
        $data = (new $this->model)->select([
            "production_results.*",
            "report_headers.line",
            "report_headers.report_date",
            "report_headers.schedule_date",
        ])
            ->leftJoin('report_headers', 'report_header_id', '=', 'report_headers.id')
            ->where('actual_balance', '>', 0);
            // return Helper::getEloquentSqlWithBindings($data);
        if ($request->has('report_header_id')) {
            $reportHeader = ReportHeader::find($request->get('report_header_id'));
            if ($reportHeader) {
                $newReportHeader = ReportHeader::where('line', $reportHeader->line)
                    ->where('report_date', '<', $reportHeader->report_date)
                    ->orderBy('report_date', 'desc');

                if ($request->has('move') && $request->get('move') < 0) {
                    $skip = \abs($request->get('move'));
                    $newReportHeader = $newReportHeader->skip($skip);
                }

                $newReportHeader = $newReportHeader->first();

                /* return [
                    'current' => $reportHeader,
                    'prev' => $newReportHeader,
                ]; */

                if ($newReportHeader) {
                    $data = $data->where('report_header_id', $newReportHeader->id);
                } else {
                    // kalau masuk sini, artinya, harusnya tidak ada balance set yang muncul;
                    $data = $data->where('report_header_id', null );
                }

                $data = $data->whereRaw("production_results.id in (select max(id) as id 
                                                from production_results 
                                                where report_header_id = {$newReportHeader->id} 
                                                GROUP BY model, 
                                                lot_no, 
                                                lot_qty)");
            }
        }
        // return Helper::getEloquentSqlWithBindings($data);
        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            if (!\is_null($query)) { //kalau not null, baru foreach
                $columns = $this->getColumnList();
                foreach ($query as $key => $value) {
                    if (\in_array($key, $columns)) {
                        if (isset($this->foreign[$key])) {
                            $data = $data->where($key, $value);
                        } else {
                            $data = $data->where($key, 'like', "%{$value}%");
                        }
                    }
                }
            }
        }

        // order of column
        if ($request->has('orderBy')) {
            $ascending = $request->has('ascending') && ($request->get('ascending') == 1) ?
                'asc' : 'desc';
            $data = $data->orderBy($request->get('orderBy'), $ascending);
        }
        // return Helper::getEloquentSqlWithBindings($data);
        $data = $data->paginate($request->has('limit') ? $request->limit : 15);

        // laravel collections
        $results = collect([
            'success' => true,
            'request' => $request->all()
        ]);

        return  $results->merge($data);
    }
    public function index_backup(Request $request)
    {
        // ambil data production results yg balance nya > 0;
        // yang harinya kurang dari hari ini.
        $data = (new $this->model)->select([
            "production_results.*",
            "report_headers.line",
            "report_headers.report_date",
            "report_headers.schedule_date",
        ])
            ->leftJoin('report_headers', 'report_header_id', '=', 'report_headers.id')
            ->where('actual_balance', '>', 0);

        if ($request->has('report_header_id')) {
            $reportHeader = ReportHeader::find($request->get('report_header_id'));
            if ($reportHeader) {
                $newReportHeader = ReportHeader::where('line', $reportHeader->line)
                    ->where('report_date', '<', $reportHeader->report_date)
                    ->orderBy('report_date', 'desc');

                if ($request->has('move') && $request->get('move') < 0) {
                    $skip = \abs($request->get('move'));
                    $newReportHeader = $newReportHeader->skip($skip);
                }

                $newReportHeader = $newReportHeader->first();

                /* return [
                    'current' => $reportHeader,
                    'prev' => $newReportHeader,
                ]; */

                if ($newReportHeader) {
                    $data = $data->where('report_header_id', $newReportHeader->id);
                } else {
                    // kalau masuk sini, artinya, harusnya tidak ada balance set yang muncul;
                    $data = $data->where('report_header_id', null );
                }
            }
        }

        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            if (!\is_null($query)) { //kalau not null, baru foreach
                $columns = $this->getColumnList();
                foreach ($query as $key => $value) {
                    if (\in_array($key, $columns)) {
                        if (isset($this->foreign[$key])) {
                            $data = $data->where($key, $value);
                        } else {
                            $data = $data->where($key, 'like', "%{$value}%");
                        }
                    }
                }
            }
        }

        // order of column
        if ($request->has('orderBy')) {
            $ascending = $request->has('ascending') && ($request->get('ascending') == 1) ?
                'asc' : 'desc';
            $data = $data->orderBy($request->get('orderBy'), $ascending);
        }

        $data = $data->paginate($request->has('limit') ? $request->limit : 15);

        // laravel collections
        $results = collect([
            'success' => true,
            'request' => $request->all()
        ]);

        return  $results->merge($data);
    }
}