<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrudController extends Controller
{
    protected $model;
    protected $columns;
    protected $columnList;
    protected $exceptColumns = [
        'id', 'created_at', 'updated_at'
    ];
    protected $foreignKeys = ["id", "report_header_id"];
    protected $foreign = [];

    public function __construct($model = 'App\\CrudModel')
    {
        // $this->model = $model;
        $this->columns = (new $this->model)->getColumns();
        $this->columnList = (new $this->model)->getColumnList();
        $this->requiredColumns = (new $this->model)->getRequiredColumn();
        foreach ($this->foreignKeys as $key => $val) {
            $this->foreign[$val] = 1; //ubah dari array to dict
        }
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getColumnList()
    {
        return $this->columnList;
    }

    public function getRequiredColumn()
    {
        return $this->requiredColumns;
    }

    public function getExceptColumns()
    {
        $results = [];
        foreach ($this->exceptColumns as $value) {
            # code...
            $results[$value] = $value;
        }
        return $results;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = (new $this->model)->select();

        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            if (!\is_null($query)) { //kalau not null, baru foreach
                $columns = $this->getColumnList();
                foreach ($query as $key => $value) {
                    if (\in_array($key, $columns)) {
                        if (isset($this->foreign[$key])) {
                            $data = $data->where($key, $value);
                        } else {
                            $data = $data->where($key, 'like', "%{$value}%");
                        }
                    }
                }
            }
        }

        // order of column
        if ($request->has('orderBy')) {
            $ascending = $request->has('ascending') && ($request->get('ascending') == 1) ?
                'asc' : 'desc';
            $data = $data->orderBy($request->get('orderBy'), $ascending);
        }

        $data = $data->paginate($request->has('limit') ? $request->limit : 15);

        // laravel collections
        $results = collect([
            'success' => true,
            'request' => $request->all()
        ]);

        return  $results->merge($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function validationRule(): array
    {
        $res = [];

        $validations = $this->getRequiredColumn();

        foreach ($validations as $key => $isRequired) {
            # code...
            if ($isRequired == false || \in_array($key, [
                "id", "created_at", "updated_at", "created_by"
            ])) {
                continue;
            }

            $res[$key] = "required";
        }

        return $res;
    }

    public function getValidationMsg()
    {
        $res = [];

        $validations = $this->getRequiredColumn();

        foreach ($validations as $key => $isRequired) {
            # code...
            if ($isRequired == false || \in_array($key, [
                "id", "created_at", "updated_at", "created_by"
            ])) {
                continue;
            }

            $res[$key . ".required"] = "The :attribute field is required.";
        }

        return $res;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRule(), $this->getValidationMsg());

        $allowedParams = $request->only($this->columnList);

        $newData = new $this->model;
        foreach ($allowedParams as $key => $value) {
            # code...
            $newData->{$key} = $value;
        }

        try {
            $newData->save();

            return [
                'success' => true,
                'message' => "data saved",
                'data' => $newData
            ];
        } catch (\Exception $th) {
            //throw $th;
            return response([
                'success' => false,
                'message' => $th->getMessage(),
                'data' => $newData,
                'trace' => $th->getTrace()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = (new $this->model)->find($id);

        if (!$data) {
            return response([
                'success' => false,
                'message' => "Data not found"
            ], 404);
        }

        $allowedParams = $request->only($this->columnList);

        $exceptColumns = $this->getExceptColumns();
        foreach ($allowedParams as $key => $value) {
            # kalau tidak dikecualikan, maka input
            if (!isset($exceptColumns[$key])) {
                $data->{$key} = $value;
            }
        }

        try {
            $data->save();

            return [
                'success' => true,
                'message' => "data saved",
                'data' => $data
            ];
        } catch (\Exception $th) {
            //throw $th;
            return response([
                'success' => false,
                'message' => $th->getMessage(),
                'data' => $data,
                'trace' => $th->getTrace()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = (new $this->model)->find($id);

        if (!$data) {
            return response([
                'success' => false,
                'message' => "Data not found"
            ], 404);
        }

        $deleted = $data->delete();

        return [
            'success' => ($deleted) > 0,
            'message' => "data deleted",
            'data' => $data
        ];
    }
}
