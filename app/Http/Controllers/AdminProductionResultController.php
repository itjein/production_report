<?php

namespace App\Http\Controllers;

use App\ProductionResult;
use App\ReportsHeader;
use Illuminate\Http\Request;
use DB;

class AdminProductionResultController extends CrudAdminController
{
    protected $model = 'App\\ProductionResult';
}
