<?php

namespace App\Http\Controllers;

use App\LostTimeReport;
use Illuminate\Http\Request;

class LostTimeReportController extends CrudController
{
    protected $model = "App\\LostTimeReport";
}
