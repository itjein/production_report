<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminOperationSummaryReportController extends CrudController
{
    protected $model = "App\\OperationSummary";
}
