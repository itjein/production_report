<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use Illuminate\Support\Carbon;
use DB;

class ScheduleController extends Controller
{
    //
    public function index(Request $request)
    {


        $params = $request->validate([
            'start_date' => 'date',
            'end_date' => 'date'
        ]);

        $today = ($request->has('run_date')) ? $request->run_date : Carbon::today()->toDateString();

        $data = Schedule::select();

        if ($request->has('query')) {
            $query = $request->get('query');
            $query = json_decode($query, true); //true to set the array as associative
            foreach ($query as $key => $value) {
                $data = $data->where($key, 'like', "%{$value}%");
            }
        }

        if($request->has('model')) {
            // ini untuk kepentingan combobox
            $data = $data
            ->orderBy('run_date', 'desc')
            ->where('model', 'like', "%{$request->get('model')}%");
        }

        if ($request->has('start_date') && $request->has('end_date')) {
            $data = $data->whereBetween('run_date', [$params['start_date'], $params['end_date']]);
        }

        // order of column
        if ($request->has('orderBy')) {
            $ascending = $request->has('ascending') && ($request->get('ascending') == 1) ?
                'asc' : 'desc';
            $data = $data->orderBy($request->get('orderBy'), $ascending);
        }

        $data = $data->paginate($request->has('limit') ? $request->limit : 15);

        // laravel collections
        $results = collect([
            'success' => true,
            'request' => $request->all()
        ]);

        return  $results->merge($data);
    }

    public function indexStoreProcedure(Request $request)
    {
        $params = $request->validate([
            'start_date' => 'date',
            'end_date' => 'date'
        ]);

        $today = ($request->has('run_date')) ? $request->run_date : Carbon::today()->toDateString();

        $params['start_date'] = isset($params['start_date']) ? $params['start_date'] : $today;
        $params['end_date'] = isset($params['end_date']) ? $params['end_date'] : $today;

        $data = DB::select("exec get_schedules '{$params['start_date']}', '{$params['end_date']}'");

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
