<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterIndirectOpHour;
use Illuminate\Support\Facades\DB;

class MasterIndirectOpHourController extends Controller
{
    //
    public function combo(Request $request)
    {
        $data = MasterIndirectOpHour::select([
            DB::raw('id as value'), DB::raw("name  as [text]")
        ])->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
