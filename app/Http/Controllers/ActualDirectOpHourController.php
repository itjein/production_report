<?php

namespace App\Http\Controllers;

use App\ActualDirectOpHour;
use App\OperationSummary;
use App\ProductionResult;
use App\StdTime;
use Illuminate\Http\Request;

class ActualDirectOpHourController extends CrudController
{
    protected $model = 'App\\ActualDirectOpHour';

    public function import_from_prod_res(Request $request){
        $this->validate($request, [
            "operation_summary_id" => "required",
            "report_header_id" => "required",
        ]);

        // return $stdTime = StdTime::orderBy("StdId", "desc")
        //     ->first();


        $opSummary = OperationSummary::find($request->operation_summary_id);

        if(!$opSummary) {
            return [
                "success" => false,
                "message" => "data not found!"
            ];
        }

        $prodRes = ProductionResult::where('report_header_id', $request->get('report_header_id'))
            ->where('shift', $opSummary->shift )
            ->get();

        // insert data ke 
        $data = $this->insertIntoActualDirectOpHours($prodRes, $request->operation_summary_id);

        return [
            "success" => true,
            "message" => "Data saved!",
            "data" => $data,
            // "production_results" => $prodRes,
        ];
    }

    public function insertIntoActualDirectOpHours($productionResults, $opSummaryId) {
        $results = [];
        foreach ($productionResults as $key => $prodRes) {
            $stdTime = StdTime::where("Model", $prodRes->model )
            ->orderBy("StdId", "desc")
            ->first();

            if(!$stdTime){
                $stdTime = 0;
            }else{
                $stdTime = $stdTime->TOTAL_STD_TIME;
            }
            # code...
            $newActualDirectOpHour = [
                "operation_summary_id" => $opSummaryId,
                "model" => $prodRes->model,
                "qty" => $prodRes->output,
                "standard_time" => $stdTime, 
                "total" => ($stdTime) ? $prodRes->qty * $stdTime : 0
            ];

            $results[] = ActualDirectOpHour::firstOrCreate([
                "operation_summary_id" => $opSummaryId,
                "model" => $prodRes->model,
            ], $newActualDirectOpHour );

        }

        return $results;
    }
}
