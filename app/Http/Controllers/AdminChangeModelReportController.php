<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminChangeModelReportController extends CrudAdminController
{
    protected $model = "App\\ChangeModelReport";
}
