<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BalanceSetsCondition;
use App\ReportsHeader;

class balanceReportController extends Controller
{
    public function index(Request $request){
        $search = $request->get('search');
        $data = BalanceSetsCondition::where('model','like','%'.$search.'%')->orWhere('shift','like','%'.$search.'%')->orWhere('remarks','like','%'.$search.'%')->orWhere('qty','like','%'.$search.'%')->orderBy('created_at','desc')->paginate(5);
        $data2 = ReportsHeader::get();

        return view('admin.vendor.balance_report.balance_set_condition_report',compact('search','data','data2'));
    }

    public function create(Request $request){
        $this->validate($request,[
            'qty'=>'required|integer',

        ]);


        $data=$request->except('_token','_method');

        BalanceSetsCondition::insert($data);  

        return redirect('/admin/balance_set_condition_report');
    }

    public function destroy($id){
        BalanceSetsCondition::where('id',$id)->delete();

        return redirect('/admin/balance_set_condition_report');
    }

    public function view($id){
        $view = BalanceSetsCondition::findOrFail($id);

        return view('admin.vendor.balance_report.view',compact('view'));

    }

    public function edit($id){
        $dt=BalanceSetsCondition::findOrFail($id);

        return view('admin.vendor.balance_report.edit',compact('dt'));

    }

    public function update($id, Request $request)
    {
    
        $pegawai = BalanceSetsCondition::findorFail($id);
        $pegawai->model = $request->model;
        $pegawai->qty = $request->qty;
        $pegawai->remarks = $request->remarks;
        $pegawai->shift = $request->shift;
        $pegawai->save();
        return redirect('/admin/balance_set_condition_report');
    }

    
}