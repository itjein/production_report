<?php

namespace App\Http\Controllers;

use App\MasterLossTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterLossTimeController extends Controller
{
    //
    public function combo(Request $request)
    {
        $data = MasterLossTime::select([
            DB::raw('id as value'), DB::raw("code + '. '+ description  as [text]")
        ])->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
