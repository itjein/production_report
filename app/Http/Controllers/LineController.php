<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Line;
use Illuminate\Support\Facades\DB;

class LineController extends Controller
{
    //
    public function combo(Request $request)
    {
        $data = Line::select([
            DB::raw('id as value'), DB::raw("line  as [text]")
        ])
        ->orderBy('line', 'asc')
        ->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
