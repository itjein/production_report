<?php

namespace App\Http\Controllers;

use App\ApprovalConfig;
use Illuminate\Http\Request;
use DB;

class ApprovalConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ApprovalConfig::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return ApprovalConfig::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApprovalConfig  $approvalConfig
     * @return \Illuminate\Http\Response
     */
    public function show(ApprovalConfig $approvalConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApprovalConfig  $approvalConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(ApprovalConfig $approvalConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $approvalConfig = ApprovalConfig::find($id);
        $approvalConfig->update($request->all());
        return $approvalConfig;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $approvalConfig = ApprovalConfig::find($id);
        $approvalConfig->delete();
        return 204;
    }
}
