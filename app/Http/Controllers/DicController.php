<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dic;
use Illuminate\Support\Facades\DB;

class DicController extends Controller
{
    //
    public function combo(Request $request)
    {
        $data = Dic::select([
            DB::raw('id as value'), DB::raw("dept_code + '. '+ dept_name  as [text]")
        ])->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
