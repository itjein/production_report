<?php

namespace App\Http\Controllers;

use App\Approval;
use App\ApprovalsConfig;
use App\User;
use ClosedGeneratorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;

class ApprovalController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.approval_edit');
    }

    public function checkStatusApproval($check_param)
    {
        /**
         * Rule Approval
         * status 3 as line leader hanya bisa approve 1 shift (hanya 1 approve di 1 report) 
         * status 4 as shift control bisa approve multi shift asal shift & status & report header id & user tidak sama 
         * status 5 as supervisor bisa approve non shift ( karena supervisor normal shift ) (hanya 1 approve di 1 report).
         */
        $user   = User::select(DB::raw("concat (' ( ',user_id,' ) ', name) as user_approve"));
        // $apr_status = $approval::where('report_header_id', '=', $report_id)
        //     ->where('user_approve', '=', $userId)
        //     ->count();
        // if ($apr_status != 0) {
        //     return response([
        //         "message" => "Anda Sudah Approve Report Ini !"
        //     ], 500);
        // }
        if ($check_param['user_status'] == 3) {
            $my_appr = Approval::where('report_header_id', '=', $check_param['report_header_id'])
                ->where('user_approve', '=', $check_param['user_id'])
                ->first();
            if (!is_null($my_appr)) {
                $user = $user->find($my_appr->user_approve);
                return [
                    "success" => true,
                    "count" => is_null($my_appr) ? '0' : '1',
                    "result" => $my_appr,
                    "user_approve" => $user['user_approve'],
                ];
            }

            $appr_status = Approval::where('report_header_id', '=', $check_param['report_header_id'])
                ->where('status', '=', $check_param['user_status'])
                ->where('shift', '=', $check_param['shift'])
                ->first();

            if (!is_null($appr_status)) {
                $user = $user->find($appr_status->user_approve);
                return [
                    "success" => true,
                    "count" => is_null($appr_status) ? '0' : '1',
                    "result" => $appr_status,
                    "user_approve" => $user['user_approve'],
                ];
            }

            return [
                "success" => true,
                "count" => is_null($appr_status) ? '0' : '1',
                "result" => $appr_status,
                "user_approve" => "",
            ];
        }
        if ($check_param['user_status'] == 4) {
            $appr_status = Approval::where('report_header_id', '=', $check_param['report_header_id'])
                ->where('status', '=', $check_param['user_status'])
                ->where('shift', '=', $check_param['shift'])
                ->first();

            if (!is_null($appr_status)) {
                $user = $user->find($appr_status->user_approve);
                return [
                    "success" => true,
                    "count" => is_null($appr_status) ? '0' : '1',
                    "result" => $appr_status,
                    "user_approve" => $user['user_approve'],
                ];
            }

            return [
                "success" => true,
                "count" => is_null($appr_status) ? '0' : '1',
                "result" => $appr_status,
                "user_approve" => "",
            ];
        }
        if ($check_param['user_status'] == 5) {
            $appr_status = Approval::where('report_header_id', '=', $check_param['report_header_id'])
                ->where('status', '=', $check_param['user_status'])
                ->first();

            if (!is_null($appr_status)) {
                $user = $user->find($appr_status->user_approve);
                return [
                    "success" => true,
                    "count" => is_null($appr_status) ? '0' : '1',
                    "result" => $appr_status,
                    "user_approve" => $user['user_approve'],
                ];
            }

            return [
                "success" => true,
                "count" => is_null($appr_status) ? '0' : '1',
                "result" => $appr_status,
                "user_approve" => "",
            ];
        }
        return "loncat to END checkStatusApproval";
    }

    public function approved(Request $request)
    {
        $params = $request->validate([
            'reportHeaderId' => 'required',
            'shift' => 'required'
        ]);
        $shift              = $params['shift'];
        $report_id          = $params['reportHeaderId'];
        $approval           = new Approval;
        $approvalsConfig    = new ApprovalsConfig;

        // get data user login
        $user   = Auth::user();
        $userId = $user['id'];

        // // check status approve anda 
        /**
         * dipindahkan ke method checkStatusApproval karena ada perbedaan kondisi pengecekan setiap level user
         */

        // $apr_status = $approval::where('report_header_id', '=', $report_id)
        //     ->where('user_approve', '=', $userId)
        //     ->count();
        // if ($apr_status != 0) {
        //     return response([
        //         "message" => "Anda Sudah Approve Report Ini !"
        //     ], 500);
        // }

        // check status user login
        $user_status = $approvalsConfig::select('status')->where('user_id', '=', $userId)->first();
        $user_status = $user_status['status'];

        // check status approve
        $appr_status = [];
        $check_param = [
            "user_id" => $userId,
            "user_status" => $user_status,
            "shift" => $shift,
            "report_header_id" => $report_id
        ];
        $check_status_approval = $this->checkStatusApproval($check_param);

        if ($check_status_approval['count'] != 0) {
            if ($check_status_approval['result']['user_approve'] == $userId) {
                return response([
                    "message" => "Anda sudah approve report ini di Shift " . $check_status_approval['result']['shift'] . ". ",
                    "Report_ID" => $report_id,
                    "User_Status" => $user_status,
                    "User_Id" => $userId
                ], 200);
            }
            return response([
                "message" => "Sudah di Approve Oleh " . $check_status_approval['user_approve'] . " di Shift " . $check_status_approval['result']['shift'] . " . ",
                "Report_ID" => $report_id,
                "User_Status" => $user_status,
                "User_Id" => $userId
            ], 500);
        }

        // Input Parameter to Approvals Table
        $approval->report_header_id = $report_id;
        $approval->created_by       = strval($userId);
        $approval->approval_time    = Carbon::now("Asia/Jakarta")->toDateTimeString();
        $approval->user_approve     = strval($userId);
        $approval->status           = $user_status;
        $approval->shift            = $shift;

        try {
            $approval->save();
            return [
                "message" => "Production Report Approved",
                "Report_ID" => $report_id,
                "User_Status" => $user_status,
                "User_Id" => $userId
            ];
        } catch (\Exception $th) {
            //throw $th;
            return response([
                'message' => $th->getMessage(),
                'trace' => $th->getTrace(),
                "Report_ID" => $report_id,
                "User_Status" => $user_status,
                "User_Id" => $userId
            ], 500);
        }



        return "loncat to END";




        // // check total Approval
        // $approve_count = Approval::where('report_header_id', '=', $report_id);
        // return $approve_count->get();

        // // check user ini sudah approve belum
        // $user_approved = $approval::select('user_approve')
        //     ->where('report_header_id', '=', $report_id)
        //     ->where('status', '=', $user_status);
        // return $user_approved->get();

        /*if ($user_status != '5') {

            if (count($user_approved) != "0") {
                $user_approved = $user_approved["0"]->user_approve;
                $user_approved = User::select(DB::raw("concat (' ( ',user_id,' ) ', name) as user_approve"))->find($user_approved);
                $user_approved = $user_approved['user_approve'];
            }
            return $user_approved;
            //check total yang approve di shift yang dipilih
            $approve_count = $approve_count
                ->where('shift', '=', $shift)
                ->where('status', '=', $user_status)
                ->count();
            if ($approve_count == 0) {
                try {
                    $approval->save();
                    return [
                        "message" => "Production Report Approved",
                        "Report_ID" => $report_id,
                        "Shift" => $shift,
                        "User_Status" => $user_status,
                        "User_Id" => $userId
                    ];
                } catch (\Exception $th) {
                    //throw $th;
                    return response([
                        'message' => $th->getMessage(),
                        'trace' => $th->getTrace(),
                        "Report_ID" => $report_id,
                        "Shift" => $shift,
                        "User_Status" => $user_status,
                        "User_Id" => $userId
                    ], 500);
                }
            } else {
                return response([
                    "message" => "Sudah di Approve Oleh " . $user_approved . " . ",
                    "Report_ID" => $report_id,
                    "User_Status" => $user_status,
                    "User_Id" => $userId
                ], 500);
            }
        } else {
            $approve_count = $approve_count->where('status', '=', '5')->get();
            $approve_count = count($approve_count);

            if ($approve_count == 0) {
                try {
                    $approval->save();
                    return [
                        "message" => "Production Report Approved",
                        "Report_ID" => $report_id,
                        "User_Status" => $user_status,
                        "User_Id" => $userId
                    ];
                } catch (\Exception $th) {
                    //throw $th;
                    return response([
                        'message' => $th->getMessage(),
                        'trace' => $th->getTrace(),
                        "Report_ID" => $report_id,
                        "User_Status" => $user_status,
                        "User_Id" => $userId
                    ], 500);
                }
            } else {
                $user_approved = $user_approved->get();
                if (count($user_approved) != "0") {
                    $user_approved = $user_approved["0"]->user_approve;
                    $user_approved = User::select(DB::raw("concat (' ( ',user_id,' ) ', name) as user_approve"))->find($user_approved);
                    $user_approved = $user_approved['user_approve'];
                    return response([
                        "message" => "Sudah di Approve Oleh " . $user_approved . " . ",
                        "Report_ID" => $report_id,
                        "User_Status" => $user_status,
                        "User_Id" => $userId
                    ], 500);
                }
            }
        }*/
    }

    public function getListApproval($ReportID)
    {
        $data = DB::select("SELECT concat(
            (
                SELECT user_id
                FROM users
                WHERE id = approvals.user_approve
            ), ' - ',
            (
                SELECT name
                FROM users
                WHERE id = approvals.user_approve
            )) AS user_approve, 
                   concat(
            (
                SELECT name
                FROM status_configs
                WHERE id = approvals.status
            ),' ( Shift ', shift, ' ) ') AS status,  
            approval_time
            FROM approvals
            WHERE report_header_id = $ReportID
            UNION ALL
            SELECT concat(users.user_id, ' - ', users.name) AS user_approve, 
                   concat(status_configs.name, ' ( Shift ', tbl.shift, ' ) ') AS status, 
                   tbl.created_at AS approval_time
            FROM production_results tbl
                 LEFT JOIN users ON users.name = tbl.created_by
                 LEFT JOIN approvals_config ON approvals_config.user_id = users.id
                 LEFT JOIN status_configs ON status_configs.id = approvals_config.status
                 INNER JOIN
            (
                SELECT shift, 
                       MIN(created_at) mincreated_at
                FROM production_results
                WHERE production_results.report_header_id = $ReportID
                GROUP BY production_results.shift
            ) tbl1 ON tbl1.shift = tbl.shift
            WHERE tbl1.mincreated_at = tbl.created_at
                  AND tbl.report_header_id = $ReportID");
        return $data;
    }
}
