<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MasterCategory extends CrudModel
{
    protected $table = "master_category";
    public function save(array $options = [])
    {
        $this->created_by = Auth::user()->id;
        parent::save($options);
    }
}
