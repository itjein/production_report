<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CrudModel;
use Illuminate\Support\Facades\Auth;

class ProductionResult extends CrudModel
{
    protected $table = 'production_results';

    public function report_header()
    {
        return $this->belongsTo('App\ReportHeader');
    }
}
