<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MasterLossTime extends CrudModel
{
    //
    protected $table = 'loss_times';

    public function save(array $options = [])
    {
        $this->create_user = Auth::user()->id;
        parent::save($options);
    }
}
