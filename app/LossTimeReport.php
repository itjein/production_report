<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LossTimeReport extends CrudModel
{
    protected $table = 'loss_time_reports';

    protected $casts = [
        'hours' => 'float',
        "total_time" => 'decimal:2',
        'lost' =>'float',
        'man_power' =>'float'
    ];

    public function dic(){
        // return $this->belongsTo('App\Dic', "dic", "dept_code");
    }

    public function setHoursAttributeBackup($value)
    {
        // hours = time end - time start 
        // 3600 = detik dalam satu jam
        $result = (\strtotime($this->attributes['time_end']) - \strtotime($this->attributes['time_start'])) / 3600;

        if ($result < 0) {
            $oneday = (24 * 60 * 60); //in second
            $result = (
                (\strtotime($this->attributes['time_end']) + $oneday) - \strtotime($this->attributes['time_start'])) / 3600;
        }

        $this->attributes['hours'] = \round($result, 2);
    }

    public function save(array $options = []) {
        // make sure hours always be calculated everytime save called
        // $this->setHoursAttribute("-");

        return parent::save($options);
    }
}
