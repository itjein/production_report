<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ReportsHeader;
use App\User;
use DB;

class ChangeMemberReport extends CrudModel
{
    protected $table = 'change_member_reports';

    public function member()
    {
        return $this->belongsTo('App\ReportsHeader', 'report_id', 'id');
    }
}
