<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MasterMeetingHour extends CrudModel
{
    //
    protected $table = "master_meeting_hour";

    public function save(array $options = [])
    {
        $this->create_user = Auth::user()->id;
        parent::save($options);
    }
}
