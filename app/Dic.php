<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Dic extends CrudModel
{
    protected $table = 'dic';
    public function save(array $options = [])
    {
        $this->create_user = Auth::user()->id;
        parent::save($options);
    }
}
