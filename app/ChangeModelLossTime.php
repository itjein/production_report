<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeModelLossTime extends Model
{
    protected $table = 'change_model_loss_times';
}
