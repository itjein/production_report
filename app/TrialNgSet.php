<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CrudModel;
use App\User;
use DB;

class TrialNgSet extends CrudModel
{
    protected $table = 'trial_ng_sets';
    protected $fillable = ['report_id', 'symptom', 'result', 'shift', 'created_at', 'updated_at', 'user_create'];

    public function header()
    {
        return $this->belongsTo('App\ReportsHeader');
    }
}
