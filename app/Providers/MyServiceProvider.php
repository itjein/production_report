<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use TCG\Voyager\VoyagerServiceProvider;

class MyServiceProvider extends VoyagerServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('TCG\Voyager\Database\Platforms\Mssql', 'App\Database\Platforms\Mssql');
    }
}
