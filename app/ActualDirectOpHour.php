<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActualDirectOpHour extends CrudModel
{
    protected $table = 'actual_direct_op_hours';

    protected $guarded = [
        "id", "created_at", "updated_at"
    ];

    protected $casts = [
        'standard_time' => 'float',
        'total' => 'float',
    ];

    public function setTotalAttribute()
    {
        $this->attributes['total'] = $this->attributes['qty'] * $this->attributes['standard_time'];
    }

    public function save(array $options = [])
    {
        // make sure hours always be calculated everytime save called
        $this->setTotalAttribute("-");

        return parent::save($options);
    }
}
