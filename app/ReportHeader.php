<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReportHeader extends CrudModel
{
    protected $fillable = ['report_date', 'line', 'created_by'];

    public function summaries() {
        return $this->hasMany('App\OperationSummary', 'report_header_id', 'id');
    }

    public function actual_direct_op_hours()
    {
        return $this->hasManyThrough('App\ActualDirectOpHour', 'App\OperationSummary')
            ->select([
                "model",
                DB::raw('avg(qty) as qty'),
                DB::raw('avg(standard_time) as standard_time'),
                DB::raw('avg(total) as total')
            ])
            ->groupBy('model')
            ->groupBy('report_header_id');
    }

    public function IndirectOpHours(){
        return $this
            ->hasManyThrough('App\IndirectOpHour', 'App\OperationSummary')
            ->select([
                'type',
                DB::raw('avg(value) as value'),
            ])
            ->groupBy('type')
            ->groupBy('report_header_id');
        
    }

    public function op_summary_loss_time() {
        return $this->hasManyThrough('App\OpSummaryLossTime', 'App\OperationSummary', "report_header_id", "op_summary_id")
            ->select([
                'loss_code_name',
                DB::raw('avg(total) as total'),
            ])
            ->groupBy('loss_code_name')
            ->groupBy('operation_summaries.report_header_id');
    }

    public function production_results(){
        return $this->hasMany('App\ProductionResult');
    }

    public function lost_time_reports(){
        return $this->hasMany('App\LossTimeReport');
    }
    
    public function change_model_reports(){
        return $this->hasMany('App\ChangeModelReport');
    }
    
    public function trial_ng_sets(){
        return $this->hasMany('App\TrialNgSet');
    }

    public function change_member_reports(){
        return $this->hasMany('App\ChangeMemberReport');
    }


}
