<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class DataExport implements 
    WithCustomStartCell,
    FromCollection,
    ShouldAutoSize,
    WithHeadings,
    WithEvents
{

    protected $data;

    public function __construct($data)
    {   
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        return $this->data;
    }

    public function headings(): array
    {
        $data = $this->data;
        
        $result = [];
        if(\count($data) > 0){
            foreach ($data->toArray()[0] as $key => $value) {
                $result[] = $key;
            }
        }
        return $result;
    }

    public function registerEvents(): array
    {
        return [
            
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = "A3:Z3";
                $event->sheet->getStyle($cellRange)->applyFromArray([
                    'font' => [ 'bold' => true ],
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);
                $event->sheet->setCellValue('B1', 'SJ2-04S-04F26 Rev. 3');
                // $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('SJ2-04S-04F26 Rev. 3');
            },
        ];
    }

    public function startCell(): string
    {
        return 'A3';
    }
}
