<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CrudModel;

class BalanceSetCondition extends CrudModel
{
    protected $table = 'balance_sets_condition';

    protected $fillable = ['report_id', 'model', 'qty', 'remarks', 'shift', 'created_by'];
}
