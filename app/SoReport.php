<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductionResult;
use App\Dic;
Use App\LossTime;
use App\ReportsHeader;

class SoReport extends Model
{
    protected $table = 'so_reports';

    protected $fillable = ['model', 'lot_no', 'time_start', 'time_end', 'hours', 'mp_d1', 'total_loss', 'code', 'dic', 'created_at', 'user_create', 'report_id'];

    public function prod(){
        return $this->belongsTo('App\ProductionResult', 'model', 'lot_no');
    }

    public function dic(){
        return $this->belongsTo('App\Dic', 'dept_name');
    }

    public function loss_time(){
        return $this->belongsTo('App\LossTime', 'code');
    }

    public function reports(){
        return $this->belongsTo('App\ReportsHeader', 'id');
    }
}
