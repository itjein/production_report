<?php

namespace App;

// use App\Approvals;
use Illuminate\Database\Eloquent\Model;

class Approval extends CrudModel
{
    protected $table = 'approvals';
    protected $fillable = ['report_header_id', 'created_by', 'approval_time', 'user_approve', 'status', 'shift'];

    // public function reports_header()
    // {
    //     return $this->BelongsTo('App\ReportsHeader', 'approve_id', 'id');
    // }
}
