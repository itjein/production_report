<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OperationSummary extends CrudModel
{
    //
    protected $table = 'operation_summaries';

    protected $casts = [
        'report_header_id' => 'integer',
        'efficiency' => 'float',
        'rate_of_operation' => 'float',
        'productivity' => 'float',
        'rate_of_attendance' => 'float',
        'working_hours' => 'float',
        'fixed_time' => 'float',
        'mp_d1' => 'float',
        'mp_d2' => 'float',
        'cl_eo' => 'float',
        'overtime' => 'float',
        'assist_other_dept_plus' => 'float',
        'assist_other_dept_min' => 'float',
        'assist_plus' => 'float',
        'assist_min' => 'float',
        'tph' => 'float',
        'loss_time_operation_summaries_total' => 'float',
        'indirect_op_hours_total' => 'float',
        'so' => 'float',
        'absence_d1' => 'float',
        'absence_d2' => 'float',
    ];

    public function IndirectOpHours()
    {
        return $this->hasMany('App\IndirectOpHour');
    }

    public function op_summary_loss_time()
    {
        return $this->hasMany("App\OpSummaryLossTime", "op_summary_id", "id");
    }

    public function getIndirectOpHourTotal()
    {
        $id = $this->id;

        return $this->IndirectOpHours()
            ->where('operation_summary_id', $id)
            ->groupBy('operation_summary_id')
            ->sum('value');
    }

    public function getALandML()
    {
        $id = $this->id;

        return $this->IndirectOpHours()
            ->where('operation_summary_id', $id)
            ->whereIn('type', [
                'ANNUAL LEAVE', 'MEDICAL LEAVE'
            ])
            ->groupBy('operation_summary_id')
            ->sum('value');
    }

    public function initIndirectOpHoursTotal()
    {
        $this->indirect_op_hours_total = $this->getIndirectOpHourTotal();
    }

    public function initLossTime()
    {

        /* $query = "SELECT sum( [total] ) as total
            FROM [production_reports].[dbo].[op_summary_loss_time]
            where op_summary_id = {$this->id}"; */

        $total = DB::table('op_summary_loss_time')->where('op_summary_id', $this->id)
            ->where('loss_code_name', '!=', 'I. SO')
            ->sum('total');

        $this->loss_time_operation_summaries_total = number_format((float) $total, 2, '.', '');
    }

    public function initTioh()
    {
        $this->tioh = $this->loss_time_operation_summaries_total + $this->indirect_op_hours_total;
    }

    public function initDwh()
    {
        $this->dwh = $this->tph - $this->tioh;
    }

    public function actual_direct_op_hours()
    {
        return $this->hasMany('App\ActualDirectOpHour');
    }

    public function getTotalActualDirectOpHours()
    {
        $res =  $this->actual_direct_op_hours()->sum('total');
        return $res;
    }

    public function initTst()
    {
        $this->tst = $this->so + $this->getTotalActualDirectOpHours();
    }

    public function initEfficiency()
    {
        if ($this->dwh != 0) {
            $total = $this->tst / $this->dwh;
            $this->efficiency = $this->getDecimal($total);
        }
    }

    public function initRo()
    {
        if ($this->dwh != 0) {
            
            if($this->tph != 0){
                $total = $this->dwh / $this->tph;
            }else{
                $total = 0;
            }

            $this->rate_of_operation = $this->getDecimal($total);
        }
    }

    public function initProductivity()
    {
        $total = $this->attributes['rate_of_operation'] * $this->attributes['efficiency'];
        $this->productivity = $this->getDecimal($total);
    }

    public function getDecimal($total)
    {

        if (\is_float($total) == false) {
            $total = (float) $total;
        }
        return number_format($total, 2, '.', '');
    }

    public function initRateOfAttendance()
    {
        // $this->rate_of_attendance = ($this->mp_d1 + $this->mp_d2) / ($this->mp_d1 + $this->mp_d2 + $this->absence_d1 + $this->absence_d2);
        // yg dihitung yg d1 saja; bu syam 2020-07-18
        // $total = ($this->mp_d1) / ($this->mp_d1 + $this->absence_d1);
        // perhitungannya berubah per 2021-04-28
        // - $this->cl_eo => tidak dihitung as rate of attendance 
        if( $this->fixed_time == 0 ) {
            $total = '';
        } else{
            $total = ( $this->fixed_time - $this->absence_d1 - $this->getALandML() ) / $this->fixed_time;
        }
        $this->rate_of_attendance = $this->getDecimal($total);
    }

    public function initComputedValue()
    {
        $this->initIndirectOpHoursTotal();
        $this->initTioh();
        $this->initLossTime();
        $this->initDwh();
        $this->initTst();
        $this->initEfficiency();
        $this->initRo();
        $this->initProductivity();
        $this->initRateOfAttendance();

        $this->save();
    }


    public function setTphAttribute($val)
    {
        $attr = &$this->attributes; //store reference not value

        $attr['tph'] = $attr['fixed_time'] + $attr['overtime']
            + $attr['assist_other_dept_plus'] + $attr['assist_plus']
            - $attr['absence_d1'] - $attr['cl_eo'] - $attr['assist_other_dept_min']
            - $attr['assist_min'];
    }

    public function setFixedTimeAttribute($val)
    {
        $attr = &$this->attributes; //store reference not value

        $attr['fixed_time'] = ($attr['working_hours'] * $attr['mp_d1']);
    }



    public function save(array $options = [])
    {
        $this->setFixedTimeAttribute("-");
        $this->setTphAttribute("-");
        $this->initRateOfAttendance();
        
        return parent::save($options);
    }

    public function getRateOfOperationAttribute()
    {
        $res = $this->attributes['rate_of_operation'] * 100;

        return "$res%";
    }
    
    public function getEfficiencyAttribute()
    {
        $res = $this->attributes['efficiency'] * 100;

        return "$res%";
    }
    
    public function getRateOfAttendanceAttribute()
    {
        $res = $this->attributes['rate_of_attendance'] * 100;

        return "$res%";
    }
    
    public function getProductivityAttribute()
    {
        $res = $this->attributes['productivity'] * 100;

        return "$res%";
    }
}
