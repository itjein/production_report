<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrialNgSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trial_ng_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_header_id')->unsigned();
            $table->string('symptom', 50);
            $table->string('result', 255);
            $table->string('shift', 50);
            $table->timestamps();
            $table->string('created_by', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trial_ng_sets');
    }
}
