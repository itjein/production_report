<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_header_id')->unsigned();
            $table->string('model', 50);
            $table->string('lot_no', 50);
            $table->integer('lot_qty');
            $table->string('shift', 20);
            $table->integer('input');
            $table->integer('total_input');
            $table->integer('balance');
            $table->integer('output');
            $table->integer('total_output');
            $table->integer('actual_balance');
            $table->integer('ocs_balance');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_results');
    }
}
