<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLossTimeReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loss_time_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('report_header_id')->nullable();
            $table->unsignedInteger('production_results_id')->nullable();
            $table->string('model', 50);
            $table->string('lotno', 50);
            $table->string('time_start', 50);
            $table->string('time_end', 50);
            $table->float('hours')->nullable();
            $table->unsignedInteger('man_power')->nullable();
            $table->string('dic', 50)->nullable();
            $table->string('loss_code', 20);
            $table->string('loss_code_name', 50);
            $table->string('symptom')->nullable();
            $table->string('created_by', 60)->nullable();
            $table->string('action')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loss_time_reports');
    }
}
