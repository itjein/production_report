<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTargetToLine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lines', function (Blueprint $table) {
            //
            $table->float('efficiency')->nullable();
            $table->float('productivity')->nullable();
            $table->float('rate_of_operation')->nullable();
            $table->float('rate_of_attendance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lines', function (Blueprint $table) {
            //
            $table->dropColumn('efficiency');
            $table->dropColumn('productivity');
            $table->dropColumn('rate_of_operation');
            $table->dropColumn('rate_of_attendance');
        });
    }
}
