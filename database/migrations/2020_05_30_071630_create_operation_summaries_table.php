<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->float('efficiency')->nullable();
            $table->float('rate_of_operation')->nullable();
            $table->float('productivity')->nullable();
            $table->float('rate_of_attendance')->nullable();
            $table->integer('mp_d1')->default(0);
            $table->integer('mp_d2')->default(0);
            $table->float('working_hours')->default(0);
            $table->float('fixed_time')->nullable();
            $table->integer('absence_d1')->nullable();
            $table->integer('absence_d2')->nullable();
            $table->float('cl_eo')->nullable();
            $table->float('overtime')->nullable();
            $table->float('assist_other_dept_plus')->nullable();
            $table->float('assist_other_dept_min')->nullable();
            $table->float('assist_plus')->nullable();
            $table->float('assist_min')->nullable();
            $table->float('tph')->nullable(); //total production hour
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_summaries');
    }
}
