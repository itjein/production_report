<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupToLossTimeReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loss_time_reports', function (Blueprint $table) {
            //
            $table->string('group', 10)->nullable();
            $table->string('shift', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loss_time_reports', function (Blueprint $table) {
            //
            $table->dropColumn('group');
            $table->dropColumn('shift');
        });
    }
}
