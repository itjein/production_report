<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLossTimeOperationSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loss_time_operation_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operation_summary_id');
            $table->float('loss_time_value')->nullable();
            $table->string('loss_time_type', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loss_time_operation_summaries');
    }
}
