<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLossTimeReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loss_time_reports', function (Blueprint $table) {
            $table->string('part_no', 100)->nullable();
            $table->string('supplier', 100)->nullable();
            $table->string('supp_code', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loss_time_reports', function (Blueprint $table) {
            $table->dropColumn('part_no');
            $table->dropColumn('supplier');
            $table->dropColumn('supp_code');
        });
    }
}