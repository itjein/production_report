<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_header_id')->unsigned();
            $table->string('model', 50);
            $table->string('lot_no', 50);
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->float('hours');
            $table->integer('mp_d1');
            $table->time('total_loss');
            $table->string('loss_kode', 10);
            $table->string('dic', 10);
            $table->timestamps();
            $table->string('created_by', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_reports');
    }
}
