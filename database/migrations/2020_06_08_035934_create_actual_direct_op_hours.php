<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualDirectOpHours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_direct_op_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operation_summary_id');
            $table->string('model', 50);
            $table->integer('qty');
            $table->float('standard_time')->default(0);
            $table->float('total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_direct_op_hours');
    }
}
