<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCauseOnChangeModelReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('change_model_reports', function (Blueprint $table) {
            //
            $table->string('cause')->nullable(); //normal atau enggak
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('change_model_reports', function (Blueprint $table) {
            //
            $table->dropColumn('cause');
        });
    }
}
