<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeModelLossTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_model_loss_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_header_id')->unsigned();
            // $table->foreign('report_header_id')->references('id')->on('reports_headers')->onDelete('cascade');
            $table->string('model', 50);
            $table->string('lot_no', 50);
            $table->datetime('time_start');
            $table->datetime('time_end');
            $table->float('hours');
            $table->integer('mp_d1');
            $table->time('total_loss');
            $table->string('loss_code', 10);
            $table->string('dis', 10);
            $table->datetime('create_time');
            $table->string('create_user', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_model_loss_times');
    }
}
