<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterOperationSummariesId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operation_summaries', function (Blueprint $table) {
            //
            $table->string('shift', 15)->nullable(); //a, b,c, normal, support
            $table->date('report_date')->nullable()->useCurrent(); //a, b,c, normal, support
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operation_summaries', function (Blueprint $table) {
            //
        });
    }
}
