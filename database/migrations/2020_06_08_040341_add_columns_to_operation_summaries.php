<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOperationSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operation_summaries', function (Blueprint $table) {
            //
            $table->float('loss_time_operation_summaries_total')->default(0)->nullable();
            $table->float('indirect_op_hours_total')->default(0)->nullable();
            $table->float('tioh')->default(0)->nullable(); // loss_time_operation_summaries_total + indirect_op_hours_total 
            $table->float('dwh')->default(0)->nullable();
            $table->float('so')->default(0)->nullable();
            $table->float('tst')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operation_summaries', function (Blueprint $table) {
            $table->dropColumn('loss_time_operation_summaries_total');
            $table->dropColumn('indirect_op_hours_total');
            $table->dropColumn('tioh');
            $table->dropColumn('dwh');
            $table->dropColumn('so');
            $table->dropColumn('tst');
        });
    }
}
