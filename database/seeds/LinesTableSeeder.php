<?php

use Illuminate\Database\Seeder;

class LinesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lines')->delete();
        
        \DB::table('lines')->insert(array (
            0 => 
            array (
                'id' => 1,
                'line' => 'MA1',
                'line_leader' => 'ADITYA OKTAVIANA',
                'create_user' => 'ADMIN',
                'created_at' => '2020-05-05 07:09:00',
                'updated_at' => '2020-05-05 07:09:00',
            ),
            1 => 
            array (
                'id' => 2,
                'line' => 'MECA',
                'line_leader' => 'ADITYA OKTAVIANA',
                'create_user' => 'ADMIN',
                'created_at' => '2020-05-05 07:09:12',
                'updated_at' => '2020-05-05 07:09:12',
            ),
            2 => 
            array (
                'id' => 3,
                'line' => 'SMT',
                'line_leader' => 'IDIAWATI',
                'create_user' => 'ADMIN',
                'created_at' => '2020-05-05 07:09:25',
                'updated_at' => '2020-05-05 07:09:25',
            ),
        ));
        
        
    }
}