<?php

namespace Tests\Unit;

use App\LossTimeReport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestLossTimeReport extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testsetHoursAttributeCorrect()
    {
        // $this->assertTrue(true);
        $lostTime = new LossTimeReport();
        $lostTime->time_start = "17:20"; #35 menit
        $lostTime->time_end = "17:55";
        $lostTime->hours = "-";

        $res = 0.58;
        $correct = $lostTime->hours == $res;

        $this->assertTrue($correct, "$lostTime->hours == $res");
    }
    
    public function testsetHoursAttributeCorrect3()
    {
        // $this->assertTrue(true);
        $lostTime = new LossTimeReport();
        $lostTime->time_start = "06:32";
        $lostTime->time_end = "06:39";
        $lostTime->hours = "-";

        $res = 0.12;
        $correct = $lostTime->hours == $res;

        $this->assertTrue($correct, "$lostTime->hours == $res");
    }
    
    public function testsetHoursAttributeCorrect2()
    {
        // $this->assertTrue(true);
        $lostTime = new LossTimeReport();
        $lostTime->time_start = "17:00";
        $lostTime->time_end = "17:30";
        $lostTime->hours = "-";

        $res = 0.5;
        $correct = $lostTime->hours == $res;

        $this->assertTrue($correct, "$lostTime->hours == $res");
    }
}
