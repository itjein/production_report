/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
window.Vue = require("vue");

import VueRouter from "vue-router";
import {
  ServerTable,
  ClientTable,
  Event
} from "vue-tables-2";
import MainComponent from "./components/ReportHeader.vue";
import HomeComponent from "./components/HomeComponent.vue";
import ProductionResults from "./components/ProductionResults.vue";
import ChangeModelReport from "./components/ChangeModelReport.vue";
import BalanceSetCondition from './components/BalanceSetCondition.vue';
import TrialNgSet from './components/TrialNgSet.vue';
import LossTimeReport from './components/LossTimeReport.vue';
import ChangeMemberReport from './components/ChangeMemberReport.vue';
import OperationSummary from './components/OperationSummary.vue';
import VModal from 'vue-js-modal';
import IndirectOpHourLostTime from './components/IndirectOpHourLostTime.vue';
import IndirectOpHours from './components/IndirectOpHours.vue';
import ActualDirectOpHours from './components/ActualDirectOpHours.vue';
import AllReport from './components/AllReport.vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("example", require("./components/Example.vue"));

// vue use client table
Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(ServerTable);
Vue.use(Event);
Vue.use(VModal);
/* 
"loss_time_reports",
"change_model_reports",
"trial_ng_sets",
"change_member_report",
"balance_set_conditions",
"operation_summaries",
*/
const routes = [{
    path: "/",
    component: MainComponent
  },{
    path: "/home",
    component: HomeComponent,
    children: [
      {
          path: "/",
          name: 'all',
          component: AllReport
      },
      {
        path: "/production_results",
        name: 'production_results',
        component: ProductionResults
      }, {
        path: "/loss_time_reports",
        name: 'loss_time_reports',
        component: LossTimeReport
      }, {
        path: "/change_model_reports",
        name: 'change_model_reports',
        component: ChangeModelReport
      }, {
        path: "/trial_ng_sets",
        name: 'trial_ng_sets',
        component: TrialNgSet
      }, {
        path: "/change_member_report",
        name: 'change_member_report',
        component: ChangeMemberReport
      }, {
        path: "/balance_set_conditions",
        name: 'balance_set_conditions',
        component: BalanceSetCondition
      }, {
        path: "/operation_summaries",
        name: 'operation_summaries',
        component: OperationSummary,
        children: [{
            path: 'indirect_op_hour_lost_time',
            name: 'indirect_op_hour_lost_time',
            component: IndirectOpHourLostTime,
          },
          {
            path: 'indirect_op_hours',
            name: 'indirect_op_hours',
            component: IndirectOpHours,
          },
          {
            path: 'actual_direct_op_hours',
            name: 'actual_direct_op_hours',
            component: ActualDirectOpHours,
          },
        ]
      }
    ]
  },
  
];

const router = new VueRouter({
  routes, // short for `routes: routes`
});

const app = new Vue({
  el: "#app",
  router,
});