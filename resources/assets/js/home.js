/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
window.Vue = require("vue");

import VueRouter from "vue-router";
import {
    ServerTable,
    ClientTable,
    Event
} from "vue-tables-2";
import MainComponent from "./components/MainComponent.vue";
import VModal from 'vue-js-modal';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("example", require("./components/Example.vue"));

// vue use client table
Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(ServerTable);
Vue.use(Event);
Vue.use(VModal);
/* 
"loss_time_reports",
"change_model_reports",
"trial_ng_sets",
"change_member_report",
"balance_set_conditions",
"operation_summaries",
*/
const routes = [{
        path: "/",
        component: MainComponent
    },
];

const router = new VueRouter({
    routes, // short for `routes: routes`
});

const app = new Vue({
    el: "#app",
    router,
});