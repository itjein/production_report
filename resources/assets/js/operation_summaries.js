/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./admin-bootstrap");
window.Vue = require("vue");

import VueRouter from "vue-router";
import {
    ServerTable,
    ClientTable,
    Event
} from "vue-tables-2";

import Main from './components/admin/Main.vue';
import VModal from 'vue-js-modal';
// import IndirectOpHourLostTimeInfo from './components/admin/IndirectOpHourLostTimeInfo.vue';
import IndirectOpHoursInfo from './components/IndirectOpHours.vue';
import ActualDirectOpHoursInfo from './components/ActualDirectOpHours.vue';

Vue.component("example", require("./components/Example.vue"));

// vue user client table
Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(ServerTable);
Vue.use(Event);
Vue.use(VModal);

const routes = [{
        path: "/",
        name: 'main',
        component: Main
    },
]

const router = new VueRouter({
    routes,
});

const admin = new Vue({
    el: "#app",
    router
});