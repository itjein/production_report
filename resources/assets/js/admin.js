/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./admin-bootstrap");
window.Vue = require("vue");

import VueRouter from "vue-router";
import {
    ServerTable,
    ClientTable,
    Event
} from "vue-tables-2";

import Main from './components/admin/ReportHeaderList.vue';
import ApproveReport from './components/admin/ApproveReport.vue';
import VModal from 'vue-js-modal';
// import IndirectOpHourLostTimeInfo from './components/admin/IndirectOpHourLostTimeInfo.vue';
import IndirectOpHoursInfo from './components/admin/IndirectOpHoursInfo.vue';
import ActualDirectOpHoursInfo from './components/admin/ActualDirectOpHoursInfo.vue';

Vue.component("example", require("./components/Example.vue"));

// vue user client table
Vue.use(VueRouter);
Vue.use(ClientTable);
Vue.use(ServerTable);
Vue.use(Event);
Vue.use(VModal);

const routes = [{
        path: "/",
        name: 'Report Header',
        component: Main
    },
    {
        path: "/approve_report",
        name: 'Approve Report',
        component: ApproveReport,
        children: [
            // {
            //     path: 'indirect_op_hour_lost_time_info',
            //     component: IndirectOpHourLostTimeInfo
            // },
            {
                path: 'indirect_op_hours_info',
                component: IndirectOpHoursInfo
            },
            {
                path: 'actual_direct_op_hours_info',
                component: ActualDirectOpHoursInfo
            }
        ]
    }
]

const router = new VueRouter({
    routes,
});

const admin = new Vue({
    el: "#app",
    router
});