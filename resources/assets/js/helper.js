import $ from 'jquery';
export default {
    getUsername() {
        return $('span#username').text() ||
            $("div.profile-body h5").text() ||
            "unknown";
    },

    getStep() {
        return 0.00001;
    },

    getPattern(){
        return "-?\\d+(\\.\\d+)?";
    },

    getPatternErrorMessage(){
        return "INPUT DECIMAL PAKAI TITIK CONTOH : 1.2";
    },

    generateRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },

    formatChart(params, labelParam = "#in Hour:") {
        let result = {
            labels: [],
            data: [],
        }

        let data = params.data;
        let backgroundColor = [];
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            let label = element.label;
            let total = parseFloat(  element.total);

            result.labels.push(label);
            result.data.push(total)

            backgroundColor.push(this.generateRandomColor())
        }

        return {
            labels: result.labels,
            datasets: [{
                label: labelParam,
                ...result,
                backgroundColor: backgroundColor,
                borderColor: backgroundColor,
                borderWidth: 1
            }]
        }
    }
}