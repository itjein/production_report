/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("../admin-bootstrap");
window.Vue = require("vue");
import VuewRouter from 'vue-router';
// import VueAxios from 'vue-axios';
import Axios from 'axios';

Vue.use(VuewRouter, axios);

import MainComponent from './components/Main.vue';
// import Create from './components/Create.vue';
// import Update from './components/Update.vue';

// membuat router
const routes = [
    {
        name: 'main',
        path: '/',
        component: MainComponent
    },
    {
        name: 'create',
        path: '/create',
        // component: Create
    },
    {
        name: 'update',
        path: '/detail/:id',
        // component: Update
    }
]

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount("#app");

// import { ClientTable } from 'vue-tables-2';

// Vue.use(ClientTable);

// import VueRouter from 'vue-router';

// Vue.use(VueRouter);
// const v = new Vue({
//     el: "#app",
//     components: {
//         MainComponent
//     }
// });

// const approvalConfigRoutes = [
//     {
//         path: "/",
//         component: MainComponent
//     },
//     {
//         path: "/approval_config",
//         component: ApprovalConfig
//     }

// ];

// const approvalConfigRouter = new VueRouter({
//     approvalConfigRoutes
// });

// const v = new Vue({
//     el: "#app",
//     approvalConfigRouter
// });