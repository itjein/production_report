@extends('voyager::master')

@section('content')
<div class="page-content">
    @include('voyager::alerts')
    {{-- @include('voyager::dimmers') --}}
    <div id="dashboard">
        <dashboard></dashboard>
    </div>
</div>
@stop

@section('css')
<style>
    .mb {
        margin-bottom: 5px;
    }
</style>
@endsection

@section('javascript')


<script src="{{url('js/dashboard.js')}}"></script>

@stop