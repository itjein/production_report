@extends('voyager::master')

@section('content')
<div id="app">
  <router-view></router-view>
</div>
@endsection



@section('javascript')

<script src="{{url('js/trial_ng_sets.js')}}"></script>
<script>
    $(".profile").on('click',function(){
    if($( ".profile" ).index('.open') == 0){
      $(".profile").removeClass("open");
    }
    else{
      $(".profile").addClass("open");
    }
  });
</script>
@endsection