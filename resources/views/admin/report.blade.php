@extends('voyager::master')

@section('content')
<div class="container-fluid">
    <div class="row mt">
        
        <div class="col-md-12">
            @foreach ($menus as $menu)    
                @include('admin.download-panel', [
                    'title' => $menu['title'],
                    'action_url' => $menu['action_url'],
                    'lines' => $lines,
                    'multiple' => isset($menu['multiple'])?$menu['multiple'] : true,
                    'date' => isset($menu['date']) ? $menu['date'] : true,
                    'download_macro_opsum' => isset($menu['opsum']) ? $menu['opsum'] : false,
                    'download_macro_cgmdl' => isset($menu['cgmdl']) ? $menu['cgmdl'] : false,
                ])
            @endforeach
        </div>

        {{-- <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="ml">Download Operation Summary</h4>
                </div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form id="op_summary_form" method="GET" action="../download_operation_summary_reports" class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input id="op_summary_month" class="form-group form-control" type="month" name="month" data-validate-length-range="13,13"
                                placeholder="month" required>
                            <select class="form-control" name="line[]" id="op_summary_lines" multiple required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success">Download</button>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
@endsection

@section('javascript')
    <script src="../js/report.js"></script>
@endsection

@section('css')
    <style>
        .mt {
            margin-top: 10px
        }
    </style>
@endsection