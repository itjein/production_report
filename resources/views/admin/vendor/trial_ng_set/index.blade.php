@extends('voyager::master')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jam.css') }}">

@section('page_header')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4>Create Trial NG-Set </h4>
                <a style="margin-bottom:10px;" type="button" class="btn btn-success voyager-plus" data-toggle="modal" data-target="#exampleModal">
                 Add New </button>
               <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-trash"></i> <span>Bulk Delete</span></a>
              
            </div>
        </div>
        
    </div>
@stop 

@section('content')
<div class="container-fluid">
<!-- a trigger modal -->
    <!-- Modal -->
    <div class="modal modal-primary fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">CREATE TRIAL NG SET</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="trial_ng_set/create_new_report" method="post">
                    {{ csrf_field() }}
                        <label for=""><b> Report id</b></label>
                        <select class="form-control" name="report_id">
                            @foreach($dropdown as $dd)
                            <option value="{{$dd->id}}">{{$dd->id}}</option>
                            @endforeach
                        </select>
                        <label for=""><b> Symptom</b></label>
                        <input type="text" name="symptom" class="form-control" id="exampleInputEmail1" >
                        <label for=""><b>Result </b></label>
                        <input type="text" name="result" class="form-control" id="exampleInputEmail1">
                        <label for=""><b></b>Shift</label>
                        <select class="form-control" name="shift" id="exampleFormControlSelect1">
                            <option>Shift 1</option>
                            <option>Shift 2</option>
                            <option>Shift 3</option>
                        </select>
                        <input readonly class="form-control" name="user_create" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <br>
    <small>Prodection Trial-Ng-Set</small>
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="get" class="form-search">
                <div id="search-input">
                    <select id="search_key" name="key" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">
                        <option value="id">Id</option>
                        <option value="symptom">Symptom</option>
                        <option value="result">Result</option>
                        <option value="shift">Shift</option>
                        <option value="create_time">Create Time</option>
                        <option value="user_create">User Create</option>
                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-search_key-container"><span class="select2-selection__rendered" id="select2-search_key-container" title="Id">Id</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    <select id="filter" name="filter" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">
                        <option value="contains">contains</option>
                        <option value="equals">=</option>
                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter-container"><span class="select2-selection__rendered" id="select2-filter-container" title="contains">contains</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <div class="input-group col-md-12">
                        <input type="text" class="form-control" placeholder="Search" name="s" value="">
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-lg" type="submit">
                                    <i class="voyager-search"></i>
                                </button>
                            </span>
                        </div>
                </div>
            </form>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Report id</th>
                        <th>Symptom</th>
                        <th>Result</th>
                        <th>Shift</th>
                        <th>Create time</th>
                        <th>Create User</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $datas)
                    <tr>
                        <td>{{ $datas->header->id }}</td>
                        <td>{{ $datas->symptom }}</td>
                        <td>{{ $datas->result }}</td>
                        <td>{{ $datas->shift }}</td>
                        <td>{{date('d M Y',strtotime($datas->create_time))}}</td>
                        <td>{{ $datas->user_create }}</td>
                        <td>
                            <!-- <button href="" class="btn btn-primary btn-sm pull-left voyager-eye"></button> -->
                            <a href="trial_ng_set/view/{{ $datas->id }}" class="btn btn-warning btn-sm pull-left voyager-eye"></button>
                            <a href="trial_ng_set/edit/{{ $datas->id }}" class="btn btn-primary btn-sm pull-left voyager-edit"></button>
                            <a href="trial_ng_set/delete/{{ $datas->id }}"onClick="return confirm('Are you sure, you want to delete this line?')"  class="btn btn-danger pull-left voyager-trash"></a>

                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection