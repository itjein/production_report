@extends('voyager::master')

@section('page_header')

@stop

@section('content')
<!-- modal edit -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit So Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="update/{{$data->id}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

            <div class="form-group">
                <label> Model </label>
                <input type="text" name="model" class="form-control">
            </div>
        
        
        </form> 
      </div>
     

    </div>
  </div>
</div>
@endsection