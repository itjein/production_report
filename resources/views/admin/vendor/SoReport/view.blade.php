@extends('voyager::master')

@section('page_header')

@stop

@section('content')
<!-- modal view-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModelLabel">View So Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <form action="view/{{$row->id}}" method="post">  
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <td>{{ $row->model}} </td> 
                        </tr>
                        <tr>
                            <th>lot No</th>
                            <td>{{ $row->lot_no }}</td>
                        </tr>
                        <tr>
                            <th>Time Start</th>
                            <td>{{ $row->time_start }} </td>
                        </tr>
                        <tr>
                            <th>Time End</th>
                            <td>{{ $row->time_end }}</td>
                        </tr>
                        <tr>
                            <th>Hours</th>
                            <td>{{ $row->hours }}</td>
                        </tr>
                        <tr>
                            <th>Mp D1</th>
                            <td>{{ $row->mp_d1 }}</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <td>{{ $row->total_loss }}</td>
                        </tr>
                        <tr>
                            <th>Code</th>
                            <td>{{ $row->loss_time->code }}</td>
                        </tr>
                        <tr>
                                <th>Dic</th>
                                <td>{{ $row->dept_name }}</td>
                            </tr>
                    </thead>
                    </table>
                    <a href="/admin/so_report" class="btn btn-primary btn-sm voyager-double-left">Back</a>
                </form>

            </div>
        </div>

      </div>

      
    </div>
  </div>
</div>
@endsection