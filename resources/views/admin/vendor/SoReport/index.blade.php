@extends('voyager::master')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jam.css') }}">
<!-- minified jquery -->
<link rel="stylesheet" href="jquery.datetimepicker.min.css">

<!-- <script src="js/jquery.js"></script> -->
<!-- <script src="jquery.datetimepicker.full.js"></script> -->
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#datetime, #datetime2").datetimepicker({ dateFormat: 'yyyy-mm-dd'}).val();
        
        console.log("testing")
    });
    </script>

@section('page_header')
<div class = "container">
    <div class = "row">
        <div class = "col-md-4">
        <h4>SO Report</h4></br>

<!-- Button Triger Modal -->
    <button style="margin-bottom:10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Add New
    </button>
    <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-trash pull_right"></i> <span>Bulk Delete</span></a>
</div>
    </div>    
        </div>

@stop

@section('content')
<div class= "container-fluid">

<!-- modal add-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="so_report/create_new_report" method="post">
        {{ csrf_field() }}

        <label for=""><b>Report Id </b> </label>
        <select class="form-control" name="report_id">
            @foreach ($rep as $dd)
            <option value="{{$dd->id}}">{{$dd->id}}</option>                        
            @endforeach
            </select>

        <label for=""><b> Model </b></label>
        <select class="form-control" name="model">
            @foreach ($rows as $dd)
            <option value="{{ $dd->id }}">{{$dd->model}} </option>
            @endforeach
        </select>
        
        <label for=""><b> Lot Number </b></label>
        <select class="form-control" name="lot_no">
            @foreach ($rows as $dd)
            <option value="{{ $dd->id }}">{{$dd->lot_no}} </option>
            @endforeach
        </select>

       <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <label for=""><b> Start Time </b></label>
                <Input id="datetime" name="time_start">

            </div>
            <div class="col-sm-4">
                <label for="">End Time</label>
                <Input id="datetime2" name="time_end">
            </div>
            <div class="col-sm-4">
                <label for="">Hours</label>
                <input type="text" name="hours" class="form-control" placeholder="Total Time" id="hasil">
            </div>
        </div>
       </div>
        
       <label for=""><b>Mp D1 </b></label>
       <input type="text" name="mp_d1" class="form-control" id="exampleInputEmail1">

       <label for=""><b> Total </b></label>
       <input type="text" name="total_loss" class="form-control" id="exampleInputEmail1">

       <label for=""><b> Code </b></label>
       <select class="form-control" name="code" placeholder="Choose">
            @foreach ($row as $dd)
            <option value="{{ $dd->id }}">{{$dd->code}} </option>
            @endforeach
      </select>
       <label for=""><b> Dic </b></label>
        <select class="form-control" name="dic" placeholder="Choose">
            @foreach ($dep as $dd)
            <option value="{{ $dd->id }}">{{$dd->dept_name}} </option>
            @endforeach
        </select>

            <input readonly class="form-control" name="user_create" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

<div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Lot No</th>        
                        <th>Time Start</th>   
                        <th>Time End</th>  
                        <th>Hours</th> 
                        <th>Mp D1</th> 
                        <th>Total</th>  
                        <th>Code</th>
                        <th>DIC</th>   
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $row) 
                    <tr>
                        <td>{{ $row->model }}</td> 
                        <td>{{ $row->lot_no }}</td>
                        <td>{{ $row->time_start }}</td> 
                        <td>{{ $row->time_end }}</td>
                        <td>{{ $row->hours }} </td>
                        <td>{{ $row->mp_d1 }}</td>
                        <td>{{ $row->total_loss }}</td>
                        <td>{{ $row->code }}</td>    
                        <td>{{ $row->dept_name }}</td>

                        <td>
                        <button href ="so_report/view/{{$row->id}}" class="btn btn-success btn-sm modalMd" data-toggle="modal" data-target="#exampleModalView">
                        <i class="voyager-eye"></i>
                        </button>
                        <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#exampleModal">
                        <i class="voyager-edit"></i>
                        </button>
                        <button class="btn btn-danger btn-sm" onClick="return confirm('Confirm Delete?')" type="submit" data-toogle="modal" data-target="exampleModaldelete">
                        <i class="voyager-trash"></i>
                        </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
 
</div>
@endsection