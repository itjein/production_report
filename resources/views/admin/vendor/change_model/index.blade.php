@extends('voyager::master')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jam.css') }}">
<!-- minified jquery -->
<link rel="stylesheet" href="jquery.datetimepicker.min.css">

<!-- <script src="js/jquery.js"></script> -->
<!-- <script src="jquery.datetimepicker.full.js"></script> -->
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#datetime, #datetime2").datetimepicker();
        console.log("testing")
    });
</script>

@section('page_header')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4>Change Model Report </h4>
                <button style="margin-bottom:10px;" type="button" class="btn btn-success voyager-plus" data-toggle="modal" data-target="#exampleModal">
                 Add New Data</button>
               <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-trash"></i> <span>Bulk Delete</span></a>
              
            </div>
        </div>
        
    </div>
@stop 

@section('content')
<div class="container-fluid">
<!-- Button trigger modal -->
    <!-- Modal Add-->
    <div class="modal  modal-primary fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-center" id="exampleModalLabel">ADD CHANGE MODEL</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="change_model/create_new_model" method="post">
                    {{ csrf_field() }}
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for=""><b> Report id</b></label>
                                        <select class="form-control" name="report_id">
                                            @foreach($dropdown as $dd)
                                            <option value="{{$dd->id}}">{{$dd->id}}</option>
                                            @endforeach
                                        </select>
                                    
                                        <label for=""><b>Model</b></label>
                                        <input type="text" name="prev_model" class="form-control" id="exampleInputEmail1" >
                                       
                                        <label for=""><b></b>Next Model</label>
                                        <input type="text" name="prev_model" class="form-control" id="exampleInputEmail1" >
                                     </div>
                                </div>

                            </div>
                            <div class="container">
                                <div class="row">
                                        <div class="col-lg-4">
                                            <label for="meeting-time">Time Start</label>
                                            <input id="datetime" name="created_at" type="datetime-local" class="form-control" >                                   
                                        </div>
                                
                                    <div class="col-lg-4">
                                        <label for="meeting-time">Time end</label>
                                        <input id="datetime2"  name="time_end" type="datetime-local" class="form-control" >
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="meeting-time">Total time</label>
                                        <input id="total"  name="time_end" type="number" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="">Normal Minute</label>
                                    <input type="text" name="normal_minute" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="col-lg-4">
                                    <label for=""><b></b>Upnormal Minute</label>
                                    <input type="text" name="upnormal_minute" class="form-control" id="exampleInputEmail1">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                             <div class="row">
                                <div class="col-sm-12">
                                    <label for=""><b></b>Major Problem</label>
                                    <input type="text" name="major_problem" class="form-control" id="exampleInputEmail1" >
                                    <label for=""><b></b>PIC</label>
                                    <input type="text" name="pic" class="form-control" id="exampleInputEmail1" >                             
                                    <!-- <label for="">Create User</label> --> 
                                    <input readonly class="form-control" name="user_create" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- table -->
    <small>Prodection Change Model Report</small>
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Report id</th>
                        <th>Prev Model</th>
                        <th>Time_start</th>
                        <th>Next_model</th>
                        <th>Time_end</th>
                        <th>Total_time</th>
                        <th>Normal_minute</th>
                        <th>Upnormal_minute</th>
                        <th>Major_problem</th>
                        <th>PIC</th>
                        <th>Create time</th>
                        <th>Create User</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $datas)
                    <tr>
                        <td>{{ $datas->model_report->id }}</td>
                        <td>{{ $datas->prev_model }}</td>
                        <td>{{ $datas->time_start }}</td>
                        <td>{{ $datas->next_model }}</td>
                        <td>{{ $datas->time_end }}</td>
                        <td>{{ $datas->total_time }}</td>
                        <td>{{ $datas->normal_minute}}</td>
                        <td>{{ $datas->major_problem}}</td>
                        <td>{{ $datas->pic}}</td>
                        <!-- <td> {{ $datas->model_report->created_at}}</td> -->
                        <td>{{date('d M Y',strtotime($datas->create_time))}}</td>
                        <td>{{ $datas->user_create }}</td>
                        <td>
                            <button href="" class="btn btn-success btn-sm pull-right voyager-eye"></button>
                            <button href="" class="btn btn-warning btn-sm pull-right voyager-edit"></button>
                            <a href="/admin/change_model/delete/{{ $datas->id }}" class="btn btn-danger btn-sm pull-right voyager-trash"></a>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection