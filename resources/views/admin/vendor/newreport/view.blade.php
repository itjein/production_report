@extends('voyager::master')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <p>View Data New Report</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="/admin/report_header" class="btn btn-danger">Back</a>
            </div>
        </div>
    </div>
@stop 

@section('content')
            <div class="box-body">
               <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>:</td>
                                <td>{{$edit->id}}</td>

                                <th>Line</th>
                                <td>:</td>
                                <td>{{$edit->line_r->line}}</td>
                            
                                <th>Approved ID</th>
                                <td>:</td>
                                <td>{{$edit->approve_id}}</td>

                                <th>Create Time</th>
                                <td>:</td>
                                <td>{{date('d M Y',strtotime($edit->created_at))}}</td>

                        
                                <th>Create By</th>
                                <td>:</td>
                                <td>{{$edit->user_create}}</td>

                            </tr>
                        </tbody>
                    </table>
               </div>
            </div
@endsection