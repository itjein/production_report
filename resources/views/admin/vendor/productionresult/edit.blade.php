@extends('voyager::master')
@section('page_header')
@stop 
@section('content')
<div class="modal-primary">
    <div class="modal-dialog modal-lg" role="document" aria-hidden="true">
        <div class="modal-content"> 
            <div class="modal-header modal-primary">
                <h4 class="modal-title text-center" id="exampleModalLabel">UPDATE PRODUCTION RESULT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>    
                    <div class="panel-body">
                        <form action="update/{{$data->id}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="exampleInputPassword1">Report Id</label>
                                <input type="number" class="form-control" name="report_id" value="{{$data->report_id}}" id="exampleInputPassword1" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Model</label>
                                <input type="text" class="form-control" name="model" value="{{$data->model}}" id="exampleInputPassword1" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Lot No</label>
                                <input type="number" class="form-control" name="lot_no" value="{{$data->lot_no}}" id="exampleInputPassword1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Lot Quantity</label>
                                <input type="number" class="form-control" name="lot_qty" value="{{$data->lot_qty}}" id="exampleInputPassword1">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right ">Save Change</button>
                             <a href="/admin/production" class="btn btn-danger voyager-double-left pull-right">Back</a>
                            
                           
                        </form>
                    </div>
        </div>
    </div>

</div> 
@endsection