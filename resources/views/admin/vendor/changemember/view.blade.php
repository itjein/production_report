@extends('voyager::master')

@section('page_header')
   
@stop 

@section('content')
<div class="modal-primary"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="exampleModalLabel">DETAIL CHANGE MEMBER</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>  
                <div class="panel-body">
                    <form action="view/{{$data->id}}" method="post">
                        <table class="table table_bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <td>{{$data->id}}</td>
                                </tr>
                                <tr>
                                    <th>Report id</th>
                                    <td>{{$data->report_id}}</td>
                                </tr>
                                <tr>
                                    <th>Flag</th>
                                    <td>{{$data->flag}}</td>
                                </tr>
                                <tr>
                                    <th>Shift</th>
                                    <td>{{$data->shift}}</td>
                                </tr>
                                <tr>
                                    <th>Refrence No</th>
                                    <td>{{$data->refrence_no}}</td>
                                </tr>
                                <tr>
                                    <th>Remark</th>
                                    <td>{{$data->remark}}</td>
                                </tr>
                                <tr>
                                    <th>Create_user</th>
                                    <td>{{$data->create_user}}</td>
                                </tr>
                                <tr>
                                    <th>Created_at</th>
                                    <td>{{$data->created_at}}</td>
                                </tr>
                            </thead>
                        </table>
                            <a href="/admin/change_member" class="btn btn-primary voyager-double-left pull-right">Back</a>
                    </form>
                   
                </div>
        </div>
    </div>
</div>

@endsection