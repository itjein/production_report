@extends('voyager::master')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <p>View Data</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="/admin/balance_set_condition_report" class="btn btn-danger">Back</a>
            </div>
        </div>
    </div>
@stop 

@section('content')
            <div class="box-body">
               <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>:</td>
                                <td>{{$view->id}}</td>

                                <th>Report ID</th>
                                <td>:</td>
                                <td>{{$view->report_id}}</td>
                            
                                <th>Model</th>
                                <td>:</td>
                                <td>{{$view->model}}</td>

                                <th>Qty</th>
                                <td>:</td>
                                <td>{{$view->qty}}</td>

                        
                                <th>Remarks</th>
                                <td>:</td>
                                <td>{{$view->remarks}}</td>

                                <th>Create By</th>
                                <td>:</td>
                                <td>{{$view->user_create}}</td>



                            </tr>
                        </tbody>
                    </table>
               </div>
            </div
@endsection