@extends('voyager::master')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <p>Edit Data</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="/admin/balance_set_condition_report" class="btn btn-danger">Back</a>
            </div>
        </div>
    </div>
@stop 

@section('content')
<div class="panel panel-default">
    <div class="panel-header text-center">
         <h4>Edit Data</h4>
    </div>
    <div class="panel-body">
        <form action="update/{{$dt->id}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
            <div class="form-group">
                <label for="exampleInputEmail1">Model</label>
                <input type="text" class="form-control" name="model" value="{{$dt->model}}" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">qty</label>
                <input type="number" class="form-control" name="qty" value="{{$dt->qty}}" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Remarks</label>
                <input type="text" class="form-control" name="remarks" value="{{$dt->remarks}}" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Shift</label>
                <input type="text" class="form-control" name="shift" value="{{$dt->shift}}" id="exampleInputPassword1">
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
    
@endsection