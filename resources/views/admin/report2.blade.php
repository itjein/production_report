@extends('voyager::master')

@section('content')
<div class="container-fluid">
    <div class="row mt">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Loss Time Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdatelt"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endatelt"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="linelt" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success" onClick=downLossTime()>Download</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Change Model Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdatecm"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endatecm"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="linecm" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success" onClick=downChangeModel()>Download</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Trial NG Set Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdatetns"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endatetns"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="linetns" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success" onClick=downTrialNGSet()>Download</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Change Member Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdatecmm"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endatecmm"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="linecmm" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success" onClick=downChangeMember()>Download</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Balance Sets Condition Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdatebsc"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endatebsc"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="linebsc" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success" onClick=downBalanceSet()>Download</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Production Result Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdatepr"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endatepr"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="linepr" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success"
                                onClick=downloadProductionResult()>Download</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Operation Summary Report</div>
                <div class="panel-body">
                    <br>
                    <div class="col-md-12" align="center">
                        <form class="form-inline">
                            <label class="form-group">Filter Data</label>
                            <input class="form-group form-control" type="date" id="stdateos"
                                data-validate-length-range="13,13" placeholder="Start Date" required>
                            <input class="form-group form-control" type="date" id="endateos"
                                data-validate-length-range="13,13" placeholder="End Date" required>
                            <select class="form-group form-control" id="lineos" required>
                                @foreach ($lines as $data)
                                <option value="{{ $data->line }}">{{ $data->line }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-success"
                                onClick=downloadOperationSummary()>Download</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<style>
    .mb {
        margin-bottom: 5px;
    }

    .mt {
        margin-top: 5px;
    }
</style>
@endsection

@section('javascript')
<script>
    function downLossTime(){
        var stdate  = document.getElementById("stdatelt").value;
        var endate  = document.getElementById("endatelt").value;
        var line    = document.getElementById("linelt").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            var splitstdate = stdate.split("-");
            var stdateymd   = splitstdate[0]+''+splitstdate[1]+''+splitstdate[2];
            var splitendate = endate.split("-");
            var endateymd   = splitendate[0]+''+splitendate[1]+''+splitendate[2];
            window.open("../download_loss_time_reports?stdate="+stdateymd+"&endate="+endateymd+"&line="+line+"");
        }
    }
    function downChangeModel(){
        var stdate  = document.getElementById("stdatecm").value;
        var endate  = document.getElementById("endatecm").value;
        var line    = document.getElementById("linecm").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            var splitstdate = stdate.split("-");
            var stdateymd   = splitstdate[0]+''+splitstdate[1]+''+splitstdate[2];
            var splitendate = endate.split("-");
            var endateymd   = splitendate[0]+''+splitendate[1]+''+splitendate[2];
            window.open("../download_change_model_reports?stdate="+stdateymd+"&endate="+endateymd+"&line="+line+"");
        }
    }
    function downTrialNGSet(){
        var stdate  = document.getElementById("stdatetns").value;
        var endate  = document.getElementById("endatetns").value;
        var line    = document.getElementById("linetns").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            var splitstdate = stdate.split("-");
            var stdateymd   = splitstdate[0]+''+splitstdate[1]+''+splitstdate[2];
            var splitendate = endate.split("-");
            var endateymd   = splitendate[0]+''+splitendate[1]+''+splitendate[2];
            window.open("../download_trial_ng_set_reports?stdate="+stdateymd+"&endate="+endateymd+"&line="+line+"");
        }
    }
    function downChangeMember(){
        var stdate  = document.getElementById("stdatecmm").value;
        var endate  = document.getElementById("endatecmm").value;
        var line    = document.getElementById("linecmm").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            var splitstdate = stdate.split("-");
            var stdateymd   = splitstdate[0]+''+splitstdate[1]+''+splitstdate[2];
            var splitendate = endate.split("-");
            var endateymd   = splitendate[0]+''+splitendate[1]+''+splitendate[2];
            window.open("../download_change_member_reports?stdate="+stdateymd+"&endate="+endateymd+"&line="+line+"");
        }
    }
    function downBalanceSet(){
        var stdate  = document.getElementById("stdatebsc").value;
        var endate  = document.getElementById("endatebsc").value;
        var line    = document.getElementById("linebsc").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            var splitstdate = stdate.split("-");
            var stdateymd   = splitstdate[0]+''+splitstdate[1]+''+splitstdate[2];
            var splitendate = endate.split("-");
            var endateymd   = splitendate[0]+''+splitendate[1]+''+splitendate[2];
            window.open("../download_balance_sets_condition_reports?stdate="+stdateymd+"&endate="+endateymd+"&line="+line+"");
        }
    }
    function downloadProductionResult(){
        var stdate = document.getElementById("stdatepr").value;
        var endate = document.getElementById("endatepr").value;
        var line = document.getElementById("linepr").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            window.open("../download_production_result_reports?stdate="+stdate+"&endate="+endate+"&line="+line+"");
        }
    }
    
    function downloadOperationSummary(){
        var stdate = document.getElementById("stdateos").value;
        var endate = document.getElementById("endateos").value;
        var line = document.getElementById("lineos").value;
        if (stdate == "") {
            return false;
        }
        else if (endate == "") {
            return false;
        }
        else if (line == "") {
            return false;
        }
        else{
            window.open("../download_operation_summary_reports?stdate="+stdate+"&endate="+endate+"&line="+line+"");
        }
    }
</script>
@endsection