<div class="panel panel-info">
    <div class="panel-heading">
        <h4 class="ml">{{ $title }}</h4>
    </div>
    <div class="panel-body">
        <br>
        <div class="col-md-12" align="center">
            <form method="GET" action="{{ $action_url }}" class="form-inline">
                <label class="form-group">Filter Data</label>
                @if ($date)
                    <input class="form-group form-control" type="date" name="start_date"
                        data-validate-length-range="13,13" placeholder="Start Date" required>
                    <input class="form-group form-control" type="date" name="end_date"
                        data-validate-length-range="13,13" placeholder="End Date" required>
                @else
                    <input id="op_summary_month" class="form-group form-control" type="month" name="month"
                        data-validate-length-range="13,13" placeholder="month" required>
                @endif

                <select class="form-control" @if ($multiple) name="line[]" multiple @else name="line" @endif required>
                    @foreach ($lines as $data)
                        <option value="{{ $data->line }}">{{ $data->line }}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn btn-success">Download</button>
            </form>
        </div>
        @if ($download_macro_opsum)
            <div class="col-md-12">
                    <center>
                            Klik tombol ini untuk download tool generate operation summary >>
                            <a class="btn btn-info" 
                                href="./../../op_smry_macro/macro.xlsm" 
                                target="_blank">
                                Download Tool Generate Opeation Summary
                            </a>
                        </center>
                
            </div>
        @elseif ($download_macro_cgmdl)
            <div class="col-md-12">
                        <center>
                            Klik tombol ini untuk download tool generate change model >>
                            <a class="btn btn-info" 
                                href="./../../op_smry_macro/macro_change_model.xls" 
                                target="_blank">
                                Download Tool Generate Change Model
                            </a>
                        </center>
                
            </div>
        @endif
    </div>
</div>

@section('css')
    <style>
        .ml {
            margin: 10px;
            color: white
        }

        .panel-heading {
            padding: 2px
        }

    </style>
@endsection
