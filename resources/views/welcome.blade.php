@extends('layouts.app')

@section('content')
<div id="home">
    <router-view></router-view>
</div>
@endsection

@section('javascript')
    <script src="{{ url('js/home.js')}}"></script>
@endsection